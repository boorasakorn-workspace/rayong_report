<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DBMain_Model extends CI_Model
{
    function __construct(){
        parent::__construct();
        $this->db_qkids = $this->load->database('qkids', TRUE);
    }

	// CheckLogin
	public function checkLogin($data){		
	      $this->db->select('*');
	      $this->db->from('public.users');
	      $this->db->where('username', $data['inputUsername']);
	      $this->db->where('password', $data['inputPassword']);
	      $this->db->where('usertype', '2');
	      $query = $this->db->get();
	      if ($query->num_rows() == 1) {
	      	return $query->result();
	      }else{
	      	return false;
	      }
	}

	// Check User
	public function checkUser(){
		if( empty($this->session->userdata('ses_username')) || $this->session->userdata('ses_usertype') != 2 ){
			redirect(base_url('Admin/Dashboard/Login'));
		}
	}

	// Total Patient
	public function TotalPatient(){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT SUM(gender_male) As i_Sum_M, 
		SUM(gender_female) As i_Sum_F, 
		SUM(walkin) AS patient_walkin, 
		SUM(appoint) AS patient_appoint ";
		$from_query = "FROM public.vw_report_patient_gender ";
		$join_query = "";
		$where_query = "WHERE careprovidername != '' ";
		$after_query = "";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Total Patient on Location
	public function LocateTotalPatient(){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT SUM(gender_male) As i_Sum_M, 
		SUM(gender_female) As i_Sum_F, 
		SUM(walkin) AS patient_walkin, 
		SUM(appoint) AS patient_appoint ";
		$from_query = "FROM public.vw_report_patient_gender ";
		$join_query = "";
		$where_query = "WHERE applocation = '".$applocation."' AND q_date = CURRENT_DATE AND careprovidername != '' ";
		$after_query = "";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Total Patient on Location
	public function LocateTotalPatientAll(){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT SUM(gender_male) As i_Sum_M, 
		SUM(gender_female) As i_Sum_F, 
		SUM(walkin) AS patient_walkin, 
		SUM(appoint) AS patient_appoint ";
		$from_query = "FROM public.vw_report_patient_gender ";
		$join_query = "";
		$where_query = "WHERE applocation = '".$applocation."' AND careprovidername != '' ";
		$after_query = "";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Total Patient on Location For IntervalGraph
	public function LocateTotalPatientAll_Interval($interval){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT SUM(gender_male) As i_Sum_M, 
		SUM(gender_female) As i_Sum_F, 
		SUM(walkin) AS patient_walkin, 
		SUM(appoint) AS patient_appoint ";
		$from_query = "FROM public.vw_report_patient_gender ";
		$join_query = "";
		$where_query = "WHERE applocation = '".$applocation."' AND q_date = CURRENT_DATE - INTERVAL '".$interval." day' AND careprovidername != '' "; 
		$after_query = "";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Total Patient on Location
	public function LocateTotalPatient_Days($interval){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');
		$intervalDays = intval($interval) - 1;

		$select_query = 
		"SELECT q_date AS resultdate, SUM(gender_male) As i_Sum_M, SUM(gender_female) As i_Sum_F, 
		SUM(walkin) AS patient_walkin, SUM(appoint) AS patient_appoint ";
		$from_query = "FROM public.vw_report_patient_gender ";
		$join_query = "";
		$where_query = "WHERE applocation = '".$applocation."' AND 
		q_date >= CURRENT_DATE - INTERVAL '".$intervalDays." day' 
		AND careprovidername != '' ";
		$after_query = "GROUP BY resultdate";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Count Patient Year Stat
	public function CountPatientYear(){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT date_part('year', q_date) As i_Year, 
		SUM(gender_male+gender_female) As i_Sum , 
		SUM(walkin) AS patient_walkin, 
		SUM(appoint) AS patient_appoint ";
		$from_query = "FROM public.vw_report_patient_gender ";
		$join_query = "";
		$where_query = 
		"WHERE applocation = '".$applocation."' 
		AND careprovidername != '' ";
		$after_query = "GROUP BY date_part('year', q_date) ORDER BY date_part('year', q_date) DESC ";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Count Patient Days Stat
	public function PatientDaysStat($startDate,$endDate){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT 
		careprovidername,careprovideruid, 
		SUM(gender_male) As i_M_Sum, 
		SUM(gender_female)As i_F_Sum , 
		SUM(walkin) AS patient_walkin, 
		SUM(appoint) AS patient_appoint ";
		$from_query = "FROM public.vw_report_patient_gender ";
		$join_query = "";
		$where_query = 
		"WHERE applocation = '".$applocation."' 
		AND to_char(q_date,'YYYY-MM-DD') BETWEEN '".$startDate."' AND '".$endDate."' 
		AND careprovidername != '' ";
		$after_query = "GROUP BY careprovidername,careprovideruid ORDER BY careprovideruid ASC";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Med Scan
	public function MedScanStat($startDate,$endDate){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT careprovideruid, careprovidername, 
		to_char((SUM(time_seconds)/sum(count_patient) || 'second')::interval, 'HH24:MI:SS') AS i_time_sum ";
		$from_query = "FROM public.vw_report_scan_meddc ";
		$join_query = "";
		$where_query = 
		"WHERE applocation = '".$applocation."' 
		AND to_char(q_start,'YYYY-MM-DD') BETWEEN '".$startDate."' AND '".$endDate."' ";
		$after_query = "GROUP BY careprovideruid, careprovidername ORDER BY careprovideruid ASC ";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// All Process Time
	public function AllProcessTime($startDate,$endDate){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT careprovideruid, careprovidername, 
		to_char((SUM(scan_to_send_timeseconds) || 'second')::interval, 'HH24:MI:SS') AS send_time_sum, 
		to_char((SUM(send_to_call_timeseconds) || 'second')::interval, 'HH24:MI:SS') AS call_time_sum, 
		to_char((SUM(call_to_meddc_timeseconds) || 'second')::interval, 'HH24:MI:SS') AS meddc_time_sum ";
		if($applocation == 3){
			$select_query .= ", to_char((SUM(send_to_meddc_timeseconds) || 'second')::interval, 'HH24:MI:SS') AS send_meddc_time_sum ";
        }
		$from_query = "FROM public.vw_patient_all_process_time ";
		$join_query = "";
		$where_query = 
		"WHERE applocation = '".$applocation."' 
		AND to_char(qdate,'YYYY-MM-DD') BETWEEN '".$startDate."' AND '".$endDate."' AND careprovidername != '' ";
		$after_query = "GROUP BY careprovideruid, careprovidername ORDER BY careprovideruid ASC ";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Per Hours
	public function TotalLinePatientPerHours(){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');
		$intervalDays = date('Y-m-d');

		$select_query = 
		"SELECT
		q_date,applocation,
		male_c_less7, male_c_7to8, male_c_8to9, 
		male_c_9to10, male_c_10to11, male_c_11to12, 
		male_c_12to13, male_c_13to14, male_c_14to15, 
		male_c_15to16, male_c_16to17, male_c_17to18, 
		male_c_18to19, male_c_19to20, male_c_20to21, 
		male_c_21to22, male_c_more22, female_c_less7, 
		female_c_7to8, female_c_8to9, female_c_9to10, 
		female_c_10to11, female_c_11to12, female_c_12to13, 
		female_c_13to14, female_c_14to15, female_c_15to16, 
		female_c_16to17, female_c_17to18, female_c_18to19, 
		female_c_19to20, female_c_20to21, female_c_21to22, female_c_more22 ";
		$from_query = "FROM public.vw_report_come_timerange ";
		$where_query = "WHERE q_date = '".$intervalDays."' AND applocation = '".$applocation."'";
		// $where_query = "WHERE applocation = '".$applocation."' AND q_date = '2019-07-14' ";


		$mysql_query = $select_query.$from_query.$where_query;
		$query = $this->$dbselect->query($mysql_query);

		if($this->$dbselect->affected_rows() > 0){
			return $query;
		}else{
			return false;
		}
	}

	// Per Hours Interval
	public function TotalLinePatientPerHours_Interval($interval){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT
		q_date,applocation,
		male_c_less7, male_c_7to8, male_c_8to9, 
		male_c_9to10, male_c_10to11, male_c_11to12, 
		male_c_12to13, male_c_13to14, male_c_14to15, 
		male_c_15to16, male_c_16to17, male_c_17to18, 
		male_c_18to19, male_c_19to20, male_c_20to21, 
		male_c_21to22, male_c_more22, female_c_less7, 
		female_c_7to8, female_c_8to9, female_c_9to10, 
		female_c_10to11, female_c_11to12, female_c_12to13, 
		female_c_13to14, female_c_14to15, female_c_15to16, 
		female_c_16to17, female_c_17to18, female_c_18to19, 
		female_c_19to20, female_c_20to21, female_c_21to22, female_c_more22 ";
		$from_query = "FROM public.vw_report_come_timerange ";
		$where_query = "WHERE q_date = CURRENT_DATE - INTERVAL '".$interval." day' AND applocation = '".$applocation."'";
		// $where_query = "WHERE applocation = '".$applocation."' AND q_date = '2019-07-14' ";


		$mysql_query = $select_query.$from_query.$where_query;
		$query = $this->$dbselect->query($mysql_query);

		if($this->$dbselect->affected_rows() > 0){
			return $query;
		}else{
			return false;
		}
	}

	// Per Hours Interval_Day
	public function TotalLinePatientPerHours_Interval_Day($interval){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');
		$preInterval = intval($interval + 1);

		$select_query = 
		"SELECT 
		applocation,
		AVG(male_c_less7) AS male_c_less7 , AVG(male_c_7to8) AS male_c_7to8 , AVG(male_c_8to9) AS male_c_8to9, 
		AVG(male_c_9to10) AS male_c_9to10 , AVG(male_c_10to11) AS male_c_10to11 , AVG(male_c_11to12) AS male_c_11to12, 
		AVG(male_c_12to13) AS male_c_12to13, AVG(male_c_13to14) AS male_c_13to14 , AVG(male_c_14to15) AS male_c_14to15, 
		AVG(male_c_15to16) AS male_c_15to16 , AVG(male_c_16to17) AS male_c_16to17 , AVG(male_c_17to18) AS male_c_17to18, 
		AVG(male_c_18to19) AS male_c_18to19 , AVG(male_c_19to20) AS male_c_19to20 , AVG(male_c_20to21) AS male_c_20to21, 
		AVG(male_c_21to22) AS male_c_21to22 , AVG(male_c_more22) AS male_c_more22 , AVG(female_c_less7) AS female_c_less7, 
		AVG(female_c_7to8) AS female_c_7to8 , AVG(female_c_8to9) AS female_c_8to9 , AVG(female_c_9to10) AS female_c_9to10, 
		AVG(female_c_10to11) AS female_c_10to11 , AVG(female_c_11to12) AS female_c_11to12 , AVG(female_c_12to13) AS female_c_12to13, 
		AVG(female_c_13to14) AS female_c_13to14 , AVG(female_c_14to15) AS female_c_14to15 , AVG(female_c_15to16) AS female_c_15to16, 
		AVG(female_c_16to17) AS female_c_16to17 , AVG(female_c_17to18) AS female_c_17to18 , AVG(female_c_18to19) AS female_c_18to19, 
		AVG(female_c_19to20) AS female_c_19to20 , AVG(female_c_20to21) AS female_c_20to21 , AVG(female_c_21to22) AS female_c_21to22, 
		AVG(female_c_more22) AS female_c_more22 ";
		$from_query = "FROM public.vw_report_come_timerange ";
		$where_query = "WHERE q_date >= CURRENT_DATE - INTERVAL '".$preInterval." day' 
		AND q_date <= CURRENT_DATE - INTERVAL '".$interval." day' 
		AND applocation = '".$applocation."' ";
		// $where_query = "WHERE applocation = '".$applocation."' AND q_date = '2019-07-14' ";
		$after_query = "GROUP BY applocation";


		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);

		if($this->$dbselect->affected_rows() > 0){
			return $query;
		}else{
			return false;
		}
	}

	// Per Hours Interval_Month
	public function TotalLinePatientPerHours_Interval_Month($interval){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');
		$preInterval = intval($interval + 1);

		$select_query = 
		"SELECT 
		applocation,
		AVG(male_c_less7) AS male_c_less7 , AVG(male_c_7to8) AS male_c_7to8 , AVG(male_c_8to9) AS male_c_8to9, 
		AVG(male_c_9to10) AS male_c_9to10 , AVG(male_c_10to11) AS male_c_10to11 , AVG(male_c_11to12) AS male_c_11to12, 
		AVG(male_c_12to13) AS male_c_12to13, AVG(male_c_13to14) AS male_c_13to14 , AVG(male_c_14to15) AS male_c_14to15, 
		AVG(male_c_15to16) AS male_c_15to16 , AVG(male_c_16to17) AS male_c_16to17 , AVG(male_c_17to18) AS male_c_17to18, 
		AVG(male_c_18to19) AS male_c_18to19 , AVG(male_c_19to20) AS male_c_19to20 , AVG(male_c_20to21) AS male_c_20to21, 
		AVG(male_c_21to22) AS male_c_21to22 , AVG(male_c_more22) AS male_c_more22 , AVG(female_c_less7) AS female_c_less7, 
		AVG(female_c_7to8) AS female_c_7to8 , AVG(female_c_8to9) AS female_c_8to9 , AVG(female_c_9to10) AS female_c_9to10, 
		AVG(female_c_10to11) AS female_c_10to11 , AVG(female_c_11to12) AS female_c_11to12 , AVG(female_c_12to13) AS female_c_12to13, 
		AVG(female_c_13to14) AS female_c_13to14 , AVG(female_c_14to15) AS female_c_14to15 , AVG(female_c_15to16) AS female_c_15to16, 
		AVG(female_c_16to17) AS female_c_16to17 , AVG(female_c_17to18) AS female_c_17to18 , AVG(female_c_18to19) AS female_c_18to19, 
		AVG(female_c_19to20) AS female_c_19to20 , AVG(female_c_20to21) AS female_c_20to21 , AVG(female_c_21to22) AS female_c_21to22, 
		AVG(female_c_more22) AS female_c_more22 ";
		$from_query = "FROM public.vw_report_come_timerange ";
		$where_query = "WHERE q_date >= CURRENT_DATE - INTERVAL '".$preInterval." month' 
		AND q_date <= CURRENT_DATE - INTERVAL '".$interval." month' 
		AND applocation = '".$applocation."' ";
		// $where_query = "WHERE applocation = '".$applocation."' AND q_date = '2019-07-14' ";
		$after_query = "GROUP BY applocation";


		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);

		if($this->$dbselect->affected_rows() > 0){
			return $query;
		}else{
			return false;
		}
	}

	// Per Hours Interval_Year
	public function TotalLinePatientPerHours_Interval_Year($interval){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');
		$preInterval = intval($interval + 1);

		$select_query = 
		"SELECT 
		applocation,
		AVG(male_c_less7) AS male_c_less7 , AVG(male_c_7to8) AS male_c_7to8 , AVG(male_c_8to9) AS male_c_8to9, 
		AVG(male_c_9to10) AS male_c_9to10 , AVG(male_c_10to11) AS male_c_10to11 , AVG(male_c_11to12) AS male_c_11to12, 
		AVG(male_c_12to13) AS male_c_12to13, AVG(male_c_13to14) AS male_c_13to14 , AVG(male_c_14to15) AS male_c_14to15, 
		AVG(male_c_15to16) AS male_c_15to16 , AVG(male_c_16to17) AS male_c_16to17 , AVG(male_c_17to18) AS male_c_17to18, 
		AVG(male_c_18to19) AS male_c_18to19 , AVG(male_c_19to20) AS male_c_19to20 , AVG(male_c_20to21) AS male_c_20to21, 
		AVG(male_c_21to22) AS male_c_21to22 , AVG(male_c_more22) AS male_c_more22 , AVG(female_c_less7) AS female_c_less7, 
		AVG(female_c_7to8) AS female_c_7to8 , AVG(female_c_8to9) AS female_c_8to9 , AVG(female_c_9to10) AS female_c_9to10, 
		AVG(female_c_10to11) AS female_c_10to11 , AVG(female_c_11to12) AS female_c_11to12 , AVG(female_c_12to13) AS female_c_12to13, 
		AVG(female_c_13to14) AS female_c_13to14 , AVG(female_c_14to15) AS female_c_14to15 , AVG(female_c_15to16) AS female_c_15to16, 
		AVG(female_c_16to17) AS female_c_16to17 , AVG(female_c_17to18) AS female_c_17to18 , AVG(female_c_18to19) AS female_c_18to19, 
		AVG(female_c_19to20) AS female_c_19to20 , AVG(female_c_20to21) AS female_c_20to21 , AVG(female_c_21to22) AS female_c_21to22, 
		AVG(female_c_more22) AS female_c_more22 ";
		$from_query = "FROM public.vw_report_come_timerange ";
		$where_query = "WHERE q_date >= CURRENT_DATE - INTERVAL '".$preInterval." year' 
		AND q_date <= CURRENT_DATE - INTERVAL '".$interval." year' 
		AND applocation = '".$applocation."' ";
		// $where_query = "WHERE applocation = '".$applocation."' AND q_date = '2019-07-14' ";
		$after_query = "GROUP BY applocation";


		$mysql_query = $select_query.$from_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);

		if($this->$dbselect->affected_rows() > 0){
			return $query;
		}else{
			return false;
		}
	}

	public function TotalLinePatientPerHoursWithDays($days){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');
		$intervalDays = $days;

		$select_query = 
		"SELECT
		q_date,applocation,
		male_c_less7, male_c_7to8, male_c_8to9, 
		male_c_9to10, male_c_10to11, male_c_11to12, 
		male_c_12to13, male_c_13to14, male_c_14to15, 
		male_c_15to16, male_c_16to17, male_c_17to18, 
		male_c_18to19, male_c_19to20, male_c_20to21, 
		male_c_21to22, male_c_more22, female_c_less7, 
		female_c_7to8, female_c_8to9, female_c_9to10, 
		female_c_10to11, female_c_11to12, female_c_12to13, 
		female_c_13to14, female_c_14to15, female_c_15to16, 
		female_c_16to17, female_c_17to18, female_c_18to19, 
		female_c_19to20, female_c_20to21, female_c_21to22, female_c_more22 ";
		$from_query = "FROM public.vw_report_come_timerange ";
		$where_query = "WHERE q_date = '".$intervalDays."' AND applocation = '".$applocation."'";


		$mysql_query = $select_query.$from_query.$where_query;
		$query = $this->$dbselect->query($mysql_query);

		if($this->$dbselect->affected_rows() > 0){
			return $query;
		}else{
			return false;
		}
	}

	// Sum Process Time of Current Date
	public function CurrentProcessTime(){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT careprovideruid, careprovidername, 
		CAST ((SUM(time_seconds)/SUM(count_patient)/60) AS INTEGER) AS sum_time_minute ";
		$from_query = "FROM public.vw_report_scan_meddc ";
		$join_query = "";
		// $where_query = "WHERE applocation = '".$applocation."' AND q_start = CURRENT_DATE ";
		$where_query = "WHERE applocation = '".$applocation."' AND q_start = '2019-07-14' ";
		$after_query = "GROUP BY careprovideruid, careprovidername ORDER BY careprovideruid ASC ";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}

	// Sum Process Time of Current Date For Interval
	public function CurrentProcessTime_Interval($interval){

		$applocation = $this->session->userdata('ses_applocation');
		$dbselect = $this->session->userdata('ses_dbselect');

		$select_query = 
		"SELECT careprovideruid, careprovidername, 
		CAST ((SUM(time_seconds)/SUM(count_patient)/60) AS INTEGER) AS sum_time_minute ";
		$from_query = "FROM public.vw_report_scan_meddc ";
		$join_query = "";
		// $where_query = "WHERE applocation = '".$applocation."' AND q_start = CURRENT_DATE ";
		$where_query = "WHERE applocation = '".$applocation."' AND q_start = CURRENT_DATE - INTERVAL '".$interval." day' "; 
		$after_query = "GROUP BY careprovideruid, careprovidername ORDER BY careprovideruid ASC ";

		$mysql_query = $select_query.$from_query.$join_query.$where_query.$after_query;
		$query = $this->$dbselect->query($mysql_query);
		return $query;
	}
}
?>