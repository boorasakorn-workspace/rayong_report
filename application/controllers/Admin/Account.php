<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
    }

    public function Login(){
        $inputPost = $this->input->post();
        $this->load->model('DBMain_Model');
        $result = $this->DBMain_Model->checkLogin($inputPost);
        if($result != false){
                foreach ($result as $row){
                    switch ($row->applocation) {
                        case '1':
                            $locationtitle = "Med";
                            $dbselect = "db";
                            break;
                        case '2':
                            $locationtitle = "Ortho";
                            $dbselect = "db";
                            break;
                        case '3':
                            $locationtitle = "Kids";
                            $dbselect = "db_qkids";
                            break;          
                    }
                    $session_data = array(
                        'ses_id' => $row->uid,
                        'ses_username' => $row->username,
                        'ses_usertype' => $row->usertype,
                        'ses_staffcode' => $row->staffcode,
                        'ses_full_name' => $row->prename." ".$row->forename." ".$row->surname,
                        'ses_applocation' => $row->applocation,
                        'ses_locationtitle' => $locationtitle,
                        'ses_dbselect' => $dbselect,
                        'ses_image' => $row->photo,
                    );
            }
            $this->session->set_userdata($session_data);
            $flashdata = "Welcome ".$this->session->userdata('ses_username')." ";
            $this->session->set_flashdata('logResult', $flashdata);

            redirect(base_url('Admin/Dashboard/Manage'));
        }
        else{
            $flashdata = "Incorrect Username or Password";
            $this->session->set_flashdata('logResult', $flashdata);
            redirect('','refresh');
        }
      
    }    

    public function Logout(){
        $this->session->sess_destroy();
        redirect(base_url('Admin/Dashboard'),'refresh');      
    }
}