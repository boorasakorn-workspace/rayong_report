<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBMain_Model');
		$this->load->model('DBMain_User', 'Acc');
	}

	public function index()
	{
		$this->load->helper('assets');
		if( !empty($this->session->userdata('ses_username')) && $this->session->userdata('ses_usertype') == 2 ){
			redirect(base_url('Admin/Dashboard/Manage'));
		}
		else{
			redirect(base_url('Admin/Dashboard/Login'));
		}
	}	

	public function Login()
	{
		$this->load->helper('assets');
		if( !empty($this->session->userdata('ses_username')) && $this->session->userdata('ses_usertype') == 2 ){
			redirect(base_url('Admin/Dashboard/Manage'));
		}
		else{
			$this->load->view('Backend/Login');
		}
	}

	public function Manage()
	{
		$this->DBMain_Model->checkUser();

		//$data['LocateTotalPatient'] = $this->DBMain_Model->LocateTotalPatient();
		
		$this->load->helper('assets');
		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';
		$data['graphChart'] = 'Backend/chart/graph-chart';
		$data['chart'] = 'src/charts_dashboard';

		$this->load->view('Backend/Dashboard', $data);
	}

	public function Overviews()
	{
		$this->DBMain_Model->checkUser();

		$data['LocateTotalPatient'] = $this->DBMain_Model->LocateTotalPatient();
		$data['GraphLocalTotalPatient'] = $this->DBMain_Model->LocateTotalPatient_Days();
		
		$this->load->helper('assets');
		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';
		$data['graphChart'] = 'Backend/chart/graph-chart';

		$this->load->view('Backend/Overview', $data);
	}

	public function TotalPatient()
	{
		$this->DBMain_Model->checkUser();

		$this->load->helper('assets');
		$data['Topic'] = "TotalPatient";
		$data['BodyContent'] = "TotalPatient";
		$data['hasDatePicker'] = 0;

		$data['Query'] = $this->DBMain_Model->TotalPatient();

		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/tableReportComponent/head', $data);
		$this->load->view('Backend/tableReportComponent/body', $data);
		$this->load->view('Backend/tableReportComponent/foot', $data);
	}

	public function LocateTotalPatient()
	{
		$this->DBMain_Model->checkUser();

		$this->load->helper('assets');
		$data['Topic'] = "LocateTotalPatient";
		$data['BodyContent'] = "LocateTotalPatient";
		$data['hasDatePicker'] = 0;

		$data['Query'] = $this->DBMain_Model->LocateTotalPatient();

		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/tableReportComponent/head', $data);
		$this->load->view('Backend/tableReportComponent/body', $data);
		$this->load->view('Backend/tableReportComponent/foot', $data);
	}

	public function CountPatientYear()
	{
		$this->DBMain_Model->checkUser();

		$this->load->helper('assets');
		$data['Topic'] = "CountPatientYear";
		$data['BodyContent'] = "CountPatientYear";
		$data['hasDatePicker'] = 0;

		$data['Query'] = $this->DBMain_Model->CountPatientYear();

		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/tableReportComponent/head', $data);
		$this->load->view('Backend/tableReportComponent/body', $data);
		$this->load->view('Backend/tableReportComponent/foot', $data);
	}

	// รางานจำนวนคนไข้
	public function PatientDaysStat()
	{
		$this->DBMain_Model->checkUser();

		$this->load->helper('assets');
		$data['Topic'] = "รายงานจำนวนคนไข้ (".$this->session->userdata('ses_locationtitle').")";
		$data['BodyContent'] = "PatientDaysStat";
		$data['hasDatePicker'] = 1;		

		$startDate = $this->input->get('start',TRUE);
		$endDate = $this->input->get('end',TRUE);

		if($startDate == NULL || $endDate == NULL){
			$startDate = date('Y-m-d');
			$endDate = date('Y-m-d');
		}else{
			$startDate = $this->_convertDate($startDate);
			$endDate = $this->_convertDate($endDate);
		}   

		$data['Query'] = $this->DBMain_Model->PatientDaysStat($startDate,$endDate);

		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/tableReportComponent/head', $data);
		$this->load->view('Backend/tableReportComponent/body', $data);
		$this->load->view('Backend/tableReportComponent/foot', $data);
	}

	// รางาน Waiting Time รวม
	public function MedScanStat()
	{
		$this->DBMain_Model->checkUser();
		
		$this->load->helper('assets');
		$data['Topic'] = "รายงาน Waiting Time รวม";
		$data['BodyContent'] = "MedScanStat";
		$data['hasDatePicker'] = 1;

		$startDate = $this->input->get('start',TRUE);
		$endDate = $this->input->get('end',TRUE);

		if($startDate == NULL || $endDate == NULL){
			$startDate = date('Y-m-d');
			$endDate = date('Y-m-d');
		}else{
			$startDate = $this->_convertDate($startDate);
			$endDate = $this->_convertDate($endDate);
		}   

		$data['Query'] = $this->DBMain_Model->MedScanStat($startDate,$endDate);

		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/tableReportComponent/head', $data);
		$this->load->view('Backend/tableReportComponent/body', $data);
		$this->load->view('Backend/tableReportComponent/foot', $data);
	}

	//รายงาน Waiting Time แยก Process
	public function AllProcessTime()
	{
		$this->DBMain_Model->checkUser();

		$this->load->helper('assets');
		$data['Topic'] = "รายงาน Waiting Time แยก Process";
		$data['BodyContent'] = "AllProcessTime";
		$data['hasDatePicker'] = 1;

		$startDate = $this->input->get('start',TRUE);
		$endDate = $this->input->get('end',TRUE);

		if($startDate == NULL || $endDate == NULL){
			$startDate = date('Y-m-d');
			$endDate = date('Y-m-d');
		}else{
			$startDate = $this->_convertDate($startDate);
			$endDate = $this->_convertDate($endDate);
		}   

		$data['Query'] = $this->DBMain_Model->AllProcessTime($startDate,$endDate);

		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/tableReportComponent/head', $data);
		$this->load->view('Backend/tableReportComponent/body', $data);
		$this->load->view('Backend/tableReportComponent/foot', $data);
	}

	public function QuantityPatient()
	{
		$this->DBMain_Model->checkUser();
		
		$this->load->helper('assets');
		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';
		$data['graphChart'] = 'Backend/chart/graph-chart';

		$this->load->view('Backend/chart/quantity-patient', $data);
	}

	function _convertDate($date){
		return date("Y-m-d", strtotime(strtr($date, '/', '-')));
	}
}