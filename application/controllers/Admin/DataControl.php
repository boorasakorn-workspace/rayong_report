<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DataControl extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBMain_Model');
	}

	public function LocateTotalPatientAll(){
		$Query = $this->DBMain_Model->LocateTotalPatientAll();
		$data['Male'] = 0;
		$data['Female'] = 0;
		$data['Sum'] = 0;
		foreach($Query->result() as $row){
			$data['Male'] += intval($row->i_sum_m);
			$data['Female'] += intval($row->i_sum_f);
			$data['Sum'] += intval($row->i_sum_m+$row->i_sum_f);
		}
		echo json_encode( $data );
	}

	public function LocateTotalPatientAll_Interval($interval){
		$Query = $this->DBMain_Model->LocateTotalPatientAll_Interval($interval);
		$data['Male'] = 0;
		$data['Female'] = 0;
		$data['Sum'] = 0;
		$data['walkin'] = 0;
		$data['appoint'] = 0;
		$data['Sum_WnA'] = 0;
		foreach($Query->result() as $row){
			$data['Male'] += intval($row->i_sum_m);
			$data['Female'] += intval($row->i_sum_f);
			$data['Sum'] += intval($row->i_sum_m+$row->i_sum_f);
			$data['walkin'] += intval($row->patient_walkin);
			$data['appoint'] += intval($row->patient_appoint);
			$data['Sum_WnA'] += intval($row->patient_walkin+$row->patient_appoint);
		}
		echo json_encode( $data );
	}

	public function LocalTotalPatient($interval)
	{
		$Query = $this->DBMain_Model->LocateTotalPatient_Days($interval);
		$i = 0;
		foreach($Query->result() as $row){
			$data[$i]['Resultdate'] = $row->resultdate;
			$data[$i]['Male'] = $row->i_sum_m;
			$data[$i]['Female'] = $row->i_sum_f;
			$data[$i]['Sum'] = $row->i_sum_m+$row->i_sum_f;
			$i++;
		}
		$i--;
		echo json_encode( $data );
	}

	public function LocalTotalPatient_Graph($interval)
	{
		$Query = $this->DBMain_Model->LocateTotalPatient_Days($interval);
		$i = 0;
		foreach($Query->result() as $row){

			$data['Resultdate'][$i] = $row->resultdate;
			$data['Male'][$i] = $row->i_sum_m;
			$data['Female'][$i] = $row->i_sum_f;
			$data['walkin'][$i] = $row->patient_walkin;
			$data['appoint'][$i] = $row->patient_appoint;
			$data['Sum_WnA'][$i] = $row->patient_walkin+$row->patient_appoint;
			$i++;
		}
		$i--;
		echo json_encode( $data );
	}

	public function CurrentProcessTime_Graph()
	{
		$Query = $this->DBMain_Model->CurrentProcessTime();
		$i = 0;
		foreach($Query->result() as $row){
			$data['careprovideruid'][$i] = $row->careprovideruid;
			$data['careprovidername'][$i] = $row->careprovidername;
			$data['sum_time_minute'][$i] = $row->sum_time_minute;
			$i++;
		}
		$i--;
		echo json_encode( $data );
	}

	public function CurrentProcessTime_Graph_Interval($interval)
	{
		$Query = $this->DBMain_Model->CurrentProcessTime_Interval($interval);
		$i = 0;
		foreach($Query->result() as $row){
			$data['careprovideruid'][$i] = $row->careprovideruid;
			$data['careprovidername'][$i] = $row->careprovidername;
			$data['sum_time_minute'][$i] = $row->sum_time_minute;
			$i++;
		}
		$i--;
		echo json_encode( $data );
	}

	public function TotalLinePatientPerHoursWithDays($interval)
	{
		
		$Query = $this->DBMain_Model->TotalLinePatientPerHoursWithDays($interval);
		if($Query){
			foreach($Query->result() as $row){
				$data['dates'] = $row->q_date;
				$data['locations'] = $row->applocation;
				$data['Male1'] = intval($row->male_c_less7);
				$data['Male2'] = intval($row->male_c_7to8);
				$data['Male3'] = intval($row->male_c_8to9);
				$data['Male4'] = intval($row->male_c_9to10);
				$data['Male5'] = intval($row->male_c_10to11);
				$data['Male6'] = intval($row->male_c_11to12);
				$data['Male7'] = intval($row->male_c_12to13);
				$data['Male8'] = intval($row->male_c_13to14);
				$data['Male9'] = intval($row->male_c_14to15);
				$data['Male10'] = intval($row->male_c_15to16);
				$data['Male11'] = intval($row->male_c_16to17);
				$data['Male12'] = intval($row->male_c_17to18);
				$data['Male13'] = intval($row->male_c_18to19);
				$data['Male14'] = intval($row->male_c_19to20);
				$data['Male15'] = intval($row->male_c_20to21);
				$data['Male16'] = intval($row->male_c_21to22);
				$data['Male17'] = intval($row->male_c_more22);
				$data['Female1'] = intval($row->female_c_less7);
				$data['Female2'] = intval($row->female_c_7to8);
				$data['Female3'] = intval($row->female_c_8to9);
				$data['Female4'] = intval($row->female_c_9to10);
				$data['Female5'] = intval($row->female_c_10to11);
				$data['Female6'] = intval($row->female_c_11to12);
				$data['Female7'] = intval($row->female_c_12to13);
				$data['Female8'] = intval($row->female_c_13to14);
				$data['Female9'] = intval($row->female_c_14to15);
				$data['Female10'] = intval($row->female_c_15to16);
				$data['Female11'] = intval($row->female_c_16to17);
				$data['Female12'] = intval($row->female_c_17to18);
				$data['Female13'] = intval($row->female_c_18to19);
				$data['Female14'] = intval($row->female_c_19to20);
				$data['Female15'] = intval($row->female_c_20to21);
				$data['Female16'] = intval($row->female_c_21to22);
				$data['Female17'] = intval($row->female_c_more22);
				$data['Total1'] = intval($row->male_c_less7)+intval($row->female_c_less7);
				$data['Total2'] = intval($row->male_c_7to8)+intval($row->female_c_7to8);
				$data['Total3'] = intval($row->male_c_8to9)+intval($row->female_c_8to9);
				$data['Total4'] = intval($row->male_c_9to10)+intval($row->female_c_9to10);
				$data['Total5'] = intval($row->male_c_10to11)+intval($row->female_c_10to11);
				$data['Total6'] = intval($row->male_c_11to12)+intval($row->female_c_11to12);
				$data['Total7'] = intval($row->male_c_12to13)+intval($row->female_c_12to13);
				$data['Total8'] = intval($row->male_c_13to14)+intval($row->female_c_13to14);
				$data['Total9'] = intval($row->male_c_14to15)+intval($row->female_c_14to15);
				$data['Total10'] = intval($row->male_c_15to16)+intval($row->female_c_15to16);
				$data['Total11'] = intval($row->male_c_16to17)+intval($row->female_c_16to17);
				$data['Total12'] = intval($row->male_c_17to18)+intval($row->female_c_17to18);
				$data['Total13'] = intval($row->male_c_18to19)+intval($row->female_c_18to19);
				$data['Total14'] = intval($row->male_c_19to20)+intval($row->female_c_19to20);
				$data['Total15'] = intval($row->male_c_20to21)+intval($row->female_c_20to21);
				$data['Total16'] = intval($row->male_c_21to22)+intval($row->female_c_21to22);
				$data['Total17'] = intval($row->male_c_more22)+intval($row->female_c_more22);
			}
			echo json_encode( $data );
		}else{
			echo json_encode("No Data");
		}
	}

	public function TotalPatientLineGraph()
	{
		$Query = $this->DBMain_Model->TotalLinePatientPerHours();
		if($Query){
			foreach($Query->result() as $row){
				$data['dates'] = $row->q_date;
				$data['locations'] = $row->applocation;
				$data['Male1'] = intval($row->male_c_less7);
				$data['Male2'] = intval($row->male_c_7to8);
				$data['Male3'] = intval($row->male_c_8to9);
				$data['Male4'] = intval($row->male_c_9to10);
				$data['Male5'] = intval($row->male_c_10to11);
				$data['Male6'] = intval($row->male_c_11to12);
				$data['Male7'] = intval($row->male_c_12to13);
				$data['Male8'] = intval($row->male_c_13to14);
				$data['Male9'] = intval($row->male_c_14to15);
				$data['Male10'] = intval($row->male_c_15to16);
				$data['Male11'] = intval($row->male_c_16to17);
				$data['Male12'] = intval($row->male_c_17to18);
				$data['Male13'] = intval($row->male_c_18to19);
				$data['Male14'] = intval($row->male_c_19to20);
				$data['Male15'] = intval($row->male_c_20to21);
				$data['Male16'] = intval($row->male_c_21to22);
				$data['Male17'] = intval($row->male_c_more22);
				$data['Female1'] = intval($row->female_c_less7);
				$data['Female2'] = intval($row->female_c_7to8);
				$data['Female3'] = intval($row->female_c_8to9);
				$data['Female4'] = intval($row->female_c_9to10);
				$data['Female5'] = intval($row->female_c_10to11);
				$data['Female6'] = intval($row->female_c_11to12);
				$data['Female7'] = intval($row->female_c_12to13);
				$data['Female8'] = intval($row->female_c_13to14);
				$data['Female9'] = intval($row->female_c_14to15);
				$data['Female10'] = intval($row->female_c_15to16);
				$data['Female11'] = intval($row->female_c_16to17);
				$data['Female12'] = intval($row->female_c_17to18);
				$data['Female13'] = intval($row->female_c_18to19);
				$data['Female14'] = intval($row->female_c_19to20);
				$data['Female15'] = intval($row->female_c_20to21);
				$data['Female16'] = intval($row->female_c_21to22);
				$data['Female17'] = intval($row->female_c_more22);
				$data['Total1'] = intval($row->male_c_less7)+intval($row->female_c_less7);
				$data['Total2'] = intval($row->male_c_7to8)+intval($row->female_c_7to8);
				$data['Total3'] = intval($row->male_c_8to9)+intval($row->female_c_8to9);
				$data['Total4'] = intval($row->male_c_9to10)+intval($row->female_c_9to10);
				$data['Total5'] = intval($row->male_c_10to11)+intval($row->female_c_10to11);
				$data['Total6'] = intval($row->male_c_11to12)+intval($row->female_c_11to12);
				$data['Total7'] = intval($row->male_c_12to13)+intval($row->female_c_12to13);
				$data['Total8'] = intval($row->male_c_13to14)+intval($row->female_c_13to14);
				$data['Total9'] = intval($row->male_c_14to15)+intval($row->female_c_14to15);
				$data['Total10'] = intval($row->male_c_15to16)+intval($row->female_c_15to16);
				$data['Total11'] = intval($row->male_c_16to17)+intval($row->female_c_16to17);
				$data['Total12'] = intval($row->male_c_17to18)+intval($row->female_c_17to18);
				$data['Total13'] = intval($row->male_c_18to19)+intval($row->female_c_18to19);
				$data['Total14'] = intval($row->male_c_19to20)+intval($row->female_c_19to20);
				$data['Total15'] = intval($row->male_c_20to21)+intval($row->female_c_20to21);
				$data['Total16'] = intval($row->male_c_21to22)+intval($row->female_c_21to22);
				$data['Total17'] = intval($row->male_c_more22)+intval($row->female_c_more22);
			}
			echo json_encode( $data );
		}else{
			echo json_encode("No Data");
		}
	}

	public function TotalPatientLineGraph_Interval($interval)
	{
		$Query = $this->DBMain_Model->TotalLinePatientPerHours_Interval($interval);
		if($Query){
			foreach($Query->result() as $row){
				$data['dates'] = $row->q_date;
				$data['locations'] = $row->applocation;
				$data['Male1'] = intval($row->male_c_less7);
				$data['Male2'] = intval($row->male_c_7to8);
				$data['Male3'] = intval($row->male_c_8to9);
				$data['Male4'] = intval($row->male_c_9to10);
				$data['Male5'] = intval($row->male_c_10to11);
				$data['Male6'] = intval($row->male_c_11to12);
				$data['Male7'] = intval($row->male_c_12to13);
				$data['Male8'] = intval($row->male_c_13to14);
				$data['Male9'] = intval($row->male_c_14to15);
				$data['Male10'] = intval($row->male_c_15to16);
				$data['Male11'] = intval($row->male_c_16to17);
				$data['Male12'] = intval($row->male_c_17to18);
				$data['Male13'] = intval($row->male_c_18to19);
				$data['Male14'] = intval($row->male_c_19to20);
				$data['Male15'] = intval($row->male_c_20to21);
				$data['Male16'] = intval($row->male_c_21to22);
				$data['Male17'] = intval($row->male_c_more22);
				$data['Female1'] = intval($row->female_c_less7);
				$data['Female2'] = intval($row->female_c_7to8);
				$data['Female3'] = intval($row->female_c_8to9);
				$data['Female4'] = intval($row->female_c_9to10);
				$data['Female5'] = intval($row->female_c_10to11);
				$data['Female6'] = intval($row->female_c_11to12);
				$data['Female7'] = intval($row->female_c_12to13);
				$data['Female8'] = intval($row->female_c_13to14);
				$data['Female9'] = intval($row->female_c_14to15);
				$data['Female10'] = intval($row->female_c_15to16);
				$data['Female11'] = intval($row->female_c_16to17);
				$data['Female12'] = intval($row->female_c_17to18);
				$data['Female13'] = intval($row->female_c_18to19);
				$data['Female14'] = intval($row->female_c_19to20);
				$data['Female15'] = intval($row->female_c_20to21);
				$data['Female16'] = intval($row->female_c_21to22);
				$data['Female17'] = intval($row->female_c_more22);
				$data['Total1'] = intval($row->male_c_less7)+intval($row->female_c_less7);
				$data['Total2'] = intval($row->male_c_7to8)+intval($row->female_c_7to8);
				$data['Total3'] = intval($row->male_c_8to9)+intval($row->female_c_8to9);
				$data['Total4'] = intval($row->male_c_9to10)+intval($row->female_c_9to10);
				$data['Total5'] = intval($row->male_c_10to11)+intval($row->female_c_10to11);
				$data['Total6'] = intval($row->male_c_11to12)+intval($row->female_c_11to12);
				$data['Total7'] = intval($row->male_c_12to13)+intval($row->female_c_12to13);
				$data['Total8'] = intval($row->male_c_13to14)+intval($row->female_c_13to14);
				$data['Total9'] = intval($row->male_c_14to15)+intval($row->female_c_14to15);
				$data['Total10'] = intval($row->male_c_15to16)+intval($row->female_c_15to16);
				$data['Total11'] = intval($row->male_c_16to17)+intval($row->female_c_16to17);
				$data['Total12'] = intval($row->male_c_17to18)+intval($row->female_c_17to18);
				$data['Total13'] = intval($row->male_c_18to19)+intval($row->female_c_18to19);
				$data['Total14'] = intval($row->male_c_19to20)+intval($row->female_c_19to20);
				$data['Total15'] = intval($row->male_c_20to21)+intval($row->female_c_20to21);
				$data['Total16'] = intval($row->male_c_21to22)+intval($row->female_c_21to22);
				$data['Total17'] = intval($row->male_c_more22)+intval($row->female_c_more22);
			}
			echo json_encode( $data );
		}else{
			echo json_encode("No Data");
		}
	}

	public function TotalPatientLineGraph_Interval_Day($interval)
	{
		$Query = $this->DBMain_Model->TotalLinePatientPerHours_Interval_Day($interval);
		if($Query){
			foreach($Query->result() as $row){
				$data['locations'] = $row->applocation;
				$data['Total1'] = intval($row->male_c_less7)+intval($row->female_c_less7);
				$data['Total2'] = intval($row->male_c_7to8)+intval($row->female_c_7to8);
				$data['Total3'] = intval($row->male_c_8to9)+intval($row->female_c_8to9);
				$data['Total4'] = intval($row->male_c_9to10)+intval($row->female_c_9to10);
				$data['Total5'] = intval($row->male_c_10to11)+intval($row->female_c_10to11);
				$data['Total6'] = intval($row->male_c_11to12)+intval($row->female_c_11to12);
				$data['Total7'] = intval($row->male_c_12to13)+intval($row->female_c_12to13);
				$data['Total8'] = intval($row->male_c_13to14)+intval($row->female_c_13to14);
				$data['Total9'] = intval($row->male_c_14to15)+intval($row->female_c_14to15);
				$data['Total10'] = intval($row->male_c_15to16)+intval($row->female_c_15to16);
				$data['Total11'] = intval($row->male_c_16to17)+intval($row->female_c_16to17);
				$data['Total12'] = intval($row->male_c_17to18)+intval($row->female_c_17to18);
				$data['Total13'] = intval($row->male_c_18to19)+intval($row->female_c_18to19);
				$data['Total14'] = intval($row->male_c_19to20)+intval($row->female_c_19to20);
				$data['Total15'] = intval($row->male_c_20to21)+intval($row->female_c_20to21);
				$data['Total16'] = intval($row->male_c_21to22)+intval($row->female_c_21to22);
				$data['Total17'] = intval($row->male_c_more22)+intval($row->female_c_more22);
			}
			echo json_encode( $data );
		}else{
			echo json_encode("No Data");
		}
	}

	public function TotalPatientLineGraph_Interval_Month($interval)
	{
		$Query = $this->DBMain_Model->TotalLinePatientPerHours_Interval_Month($interval);
		if($Query){
			foreach($Query->result() as $row){
				$data['locations'] = $row->applocation;
				$data['Total1'] = intval($row->male_c_less7)+intval($row->female_c_less7);
				$data['Total2'] = intval($row->male_c_7to8)+intval($row->female_c_7to8);
				$data['Total3'] = intval($row->male_c_8to9)+intval($row->female_c_8to9);
				$data['Total4'] = intval($row->male_c_9to10)+intval($row->female_c_9to10);
				$data['Total5'] = intval($row->male_c_10to11)+intval($row->female_c_10to11);
				$data['Total6'] = intval($row->male_c_11to12)+intval($row->female_c_11to12);
				$data['Total7'] = intval($row->male_c_12to13)+intval($row->female_c_12to13);
				$data['Total8'] = intval($row->male_c_13to14)+intval($row->female_c_13to14);
				$data['Total9'] = intval($row->male_c_14to15)+intval($row->female_c_14to15);
				$data['Total10'] = intval($row->male_c_15to16)+intval($row->female_c_15to16);
				$data['Total11'] = intval($row->male_c_16to17)+intval($row->female_c_16to17);
				$data['Total12'] = intval($row->male_c_17to18)+intval($row->female_c_17to18);
				$data['Total13'] = intval($row->male_c_18to19)+intval($row->female_c_18to19);
				$data['Total14'] = intval($row->male_c_19to20)+intval($row->female_c_19to20);
				$data['Total15'] = intval($row->male_c_20to21)+intval($row->female_c_20to21);
				$data['Total16'] = intval($row->male_c_21to22)+intval($row->female_c_21to22);
				$data['Total17'] = intval($row->male_c_more22)+intval($row->female_c_more22);
			}
			echo json_encode( $data );
		}else{
			echo json_encode("No Data");
		}
	}

	public function TotalPatientLineGraph_Interval_Year($interval)
	{
		$Query = $this->DBMain_Model->TotalLinePatientPerHours_Interval_Year($interval);
		if($Query){
			foreach($Query->result() as $row){
				$data['locations'] = $row->applocation;
				$data['Total1'] = intval($row->male_c_less7)+intval($row->female_c_less7);
				$data['Total2'] = intval($row->male_c_7to8)+intval($row->female_c_7to8);
				$data['Total3'] = intval($row->male_c_8to9)+intval($row->female_c_8to9);
				$data['Total4'] = intval($row->male_c_9to10)+intval($row->female_c_9to10);
				$data['Total5'] = intval($row->male_c_10to11)+intval($row->female_c_10to11);
				$data['Total6'] = intval($row->male_c_11to12)+intval($row->female_c_11to12);
				$data['Total7'] = intval($row->male_c_12to13)+intval($row->female_c_12to13);
				$data['Total8'] = intval($row->male_c_13to14)+intval($row->female_c_13to14);
				$data['Total9'] = intval($row->male_c_14to15)+intval($row->female_c_14to15);
				$data['Total10'] = intval($row->male_c_15to16)+intval($row->female_c_15to16);
				$data['Total11'] = intval($row->male_c_16to17)+intval($row->female_c_16to17);
				$data['Total12'] = intval($row->male_c_17to18)+intval($row->female_c_17to18);
				$data['Total13'] = intval($row->male_c_18to19)+intval($row->female_c_18to19);
				$data['Total14'] = intval($row->male_c_19to20)+intval($row->female_c_19to20);
				$data['Total15'] = intval($row->male_c_20to21)+intval($row->female_c_20to21);
				$data['Total16'] = intval($row->male_c_21to22)+intval($row->female_c_21to22);
				$data['Total17'] = intval($row->male_c_more22)+intval($row->female_c_more22);
			}
			echo json_encode( $data );
		}else{
			echo json_encode("No Data");
		}
	}

	function _convertDate($date){
		return date("Y-m-d", strtotime(strtr($date, '/', '-')));
	}
}