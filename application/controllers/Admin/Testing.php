<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Testing extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
        $this->load->model('DBMain_Model');
	}

	public function Report()
	{
		$this->DBMain_Model->checkUser();

		$this->load->model('DBMain_User', 'Acc');
		$f = $this->Acc->FetchAcc();
		foreach($f->result() as $user){
			$data['fuser'] = $user;
		}
		
		$this->load->helper('assets');
		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/Form', $data);
	}

	public function Table()
	{  

		$startDate = '2019-08-07';
		$endDate = '2019-08-07';

		$this->DBMain_Model->checkUser();

		$this->load->model('DBMain_User', 'Acc');
		$f = $this->Acc->FetchAcc();
		foreach($f->result() as $user){
			$data['fuser'] = $user;
		}
		
		$this->load->helper('assets');
		$data['TotalPatient'] = $this->DBMain_Model->TotalPatient();
		$data['LocateTotalPatient'] = $this->DBMain_Model->LocateTotalPatient();
		$data['PatientDaysStat'] = $this->DBMain_Model->PatientDaysStat($startDate,$endDate);
		$data['CountPatientYear'] = $this->DBMain_Model->CountPatientYear();
		$data['MedScanStat'] = $this->DBMain_Model->MedScanStat($startDate,$endDate);
		$data['AllProcessTime'] = $this->DBMain_Model->AllProcessTime($startDate,$endDate);

		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/Table', $data);
	}

	public function TestFullForm()
	{
		$data['Topic'] = "PatientDaysStat";
		
		$startDate = $this->input->get('start',TRUE);
		$endDate = $this->input->get('end',TRUE);
		$startDate = strtr($startDate, '/', '-');
		$endDate = strtr($endDate, '/', '-');

		if($startDate == NULL || $endDate == NULL){
			$startDate = date('Y-m-d');
			$endDate = date('Y-m-d');
		}else{
			$startDate = date("Y-m-d", strtotime($startDate));
			$endDate = date("Y-m-d", strtotime($endDate));
		}   

		$this->DBMain_Model->checkUser();

		$this->load->model('DBMain_User', 'Acc');
		$f = $this->Acc->FetchAcc();
		foreach($f->result() as $user){
			$data['fuser'] = $user;
		}
		
		$this->load->helper('assets');
		$data['TotalPatient'] = $this->DBMain_Model->TotalPatient();
		$data['LocateTotalPatient'] = $this->DBMain_Model->LocateTotalPatient();
		$data['Query'] = $this->DBMain_Model->PatientDaysStat($startDate,$endDate);
		$data['CountPatientYear'] = $this->DBMain_Model->CountPatientYear();
		$data['MedScanStat'] = $this->DBMain_Model->MedScanStat($startDate,$endDate);
		$data['AllProcessTime'] = $this->DBMain_Model->AllProcessTime($startDate,$endDate);

		$data['headercontent'] = 'src/header-content';
		$data['sidebarcontent'] = 'src/sidebar-content';

		$this->load->view('Backend/TestFullForm', $data);
	}

}