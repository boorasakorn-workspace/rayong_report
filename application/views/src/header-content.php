		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="index.html" class="navbar-brand">
					<?=assets_img('S__11313217.png', array('width'=>'130px'));?>
				</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li class="dropdown navbar-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<?php /*
						<?php echo assets_img($fuser->photo); ?>						
						<span class="d-none d-md-inline"><?php echo $fuser->username; ?></span> <b class="caret"></b>
						*/ ?>
						<?=($this->session->userdata('ses_image') != "" ? assets_img($this->session->userdata('ses_image')) : assets_img('user-12.jpg'))?>
						<span class="d-none d-md-inline"><?=$this->session->userdata('ses_username'); ?></span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<!-- <a href="javascript:;" class="dropdown-item">Edit Profile</a> -->
						<!-- <a href="javascript:;" class="dropdown-item">Setting</a> -->
						<!-- <div class="dropdown-divider"></div> -->
						<a href="<?=base_url('Admin/Account/Logout');?>" class="dropdown-item">Log Out</a>
					</div>
				</li>
			</ul>
			<!-- end header-nav -->
		</div>