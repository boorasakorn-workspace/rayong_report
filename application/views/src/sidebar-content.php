		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<!-- <a href="javascript:;" data-toggle="nav-profile"> -->
							<div class="cover with-shadow"></div>
							<div class="image">
								<?php //echo assets_img($fuser->photo); ?>
								<?=($this->session->userdata('ses_image') != "" ? assets_img($this->session->userdata('ses_image')) : assets_img('user-12.jpg'))?>
							</div>
							<div class="info">
								<!-- <b class="caret pull-right"></b> -->
								<?php //echo $fuser->username; ?>
								<?=$this->session->userdata('ses_username'); ?>
								<small>Backend Report</small>
							</div>
						<!-- </a> -->
					</li>
					<li>
						<ul class="nav nav-profile">
							<li><a href="javascript:;"><i class="fa fa-cog"></i> Settings</a></li>
						</ul>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Menu List</li>
					<!--
					<li>
						<a href="#">
							<i class="fab fa-simplybuilt"></i> 
							<span>Current Usage</span> 
						</a>
					</li>
					-->
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="fa fa-gem"></i>
							<span>Report</span> 
						</a>
						<ul class="sub-menu">
							<li><a href="<?=base_url('Admin/Dashboard/AllProcessTime');?>">รายงาน Waiting Time แยกตาม Process</a></li>
							<!-- <li><a href="<?=base_url('Admin/Dashboard/MedScanStat');?>">รายงาน Waiting Time รวม</a></li> -->
							<li><a href="<?=base_url('Admin/Dashboard/PatientDaysStat');?>">รายงานจำนวนคนไข้ (<?=$this->session->userdata('ses_locationtitle');?>)</a></li>
							<li><a href="<?=base_url('Admin/Dashboard/QuantityPatient');?>">ข้อมูลจำนวนคนไข้รายวัน (<?=$this->session->userdata('ses_locationtitle');?>)</a></li>
						</ul>
					</li>
					<li>
						<a href="<?=base_url('Admin/Dashboard/Manage');?>">
							<i class="fab fa-simplybuilt"></i> 
							<span>Dashboard</span> 
						</a>
					</li>
					<!--
					<li>
						<a href="<?=base_url('Admin/Dashboard/Overview');?>">
							<i class="fab fa-simplybuilt"></i> 
							<span>Overview</span> 
						</a>
					</li>
					-->
					<!--
					<li>
						<a href="#">
							<i class="fas fa-user"></i> 
							<span>User Management</span> 
						</a>
					</li>
					-->
					
					<!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="fa fa-angle-double-left"></i></a></li>
					<!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>