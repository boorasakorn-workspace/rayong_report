	<script>
		function DrawBarChart(Element,TitleInterval,Title,Walkin,Appoint,Total){
			//Temporary Draw Chart
			var options = {
				chart: {
					height: 350,
					type: 'bar',
					stacked: false,
				},
				dataLabels: {
					enabled: false
				},
				series: [
						{
							name: 'Walkin',
							type: 'column',
							data: Walkin,
						},
						{
							name: 'Appoint',
							type: 'column',
							data: Appoint,
						},
						{
							name: 'Total',
							type: 'column',
							data: Total,
						},
				],
				stroke: {
					width: [0, 0, 3]
				},
				colors: [COLOR_BLUE_DARKER, COLOR_TEAL, COLOR_ORANGE],
				title: {
					text: 'จำนวนคนไข้ล่าสุดใน '+TitleInterval+' Days',
					align: 'left',
					offsetX: 110
				},
				xaxis: {
					categories: Title,
					axisBorder: {
						show: true,
						color: COLOR_SILVER_TRANSPARENT_5,
						height: 1,
						width: '100%',
						offsetX: 0,
						offsetY: -1
					},
					axisTicks: {
						show: true,
						borderType: 'solid',
						color: COLOR_SILVER,
						height: 6,
						offsetX: 0,
						offsetY: 0
					}
				},
				tooltip: {
					fixed: {
						enabled: true,
						position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
						offsetY: 30,
						offsetX: 60
					},
				},
				legend: {
					horizontalAlign: 'left',
					offsetX: 40
				}
			};

			var chart = new ApexCharts(
				document.querySelector(Element),
				options
			);

			chart.render();
			//End Draw
		}

		function DrawPieChart(Element,Title,Walkin,Appoint){
			var options = {
				chart: {
					height: 386,
					type: 'pie',
				},
				dataLabels: {
					dropShadow: {
						enabled: false,
						top: 1,
						left: 1,
						blur: 1,
						opacity: 0.45
					}
				},
				colors: [COLOR_BLUE, COLOR_ORANGE],
				labels: ['Walkin', 'Appoint'],
				series: [Walkin,Appoint],
				title: {
					text: Title,
				}
			};

			var chart = new ApexCharts(
				document.querySelector(Element),
				options
			);

			chart.render();
		}

		function DrawLineChart(Element,patientPerHours){
			// console.log(maleAll.replace(/"/g, '\''));
			var options = {
			  	chart: {
				    height: 385,
				    type: "line",
				    stacked: false
				},
			  	dataLabels: {
			    	enabled: false
			  	},
			  	colors: ['orange'],
			  	series: [
			    	/*{
				      	name: 'Male',
				      	type: 'line',
				      	data: maleAll
			    	},
			    	{
				      	name: "Female",
				      	type: 'line',
				      	data: femaleAll
			    	},*/
			    	{
				      	name: "เฉลี่ยผู้ป่วยต่อชั่วโมง (คน)",
				      	type: 'line',
				      	data: patientPerHours
			    	}
			  	],
			  	stroke: {
			    	width: [4],
				    colors: [COLOR_ORANGE]
			  	},
			  	plotOptions: {
				    bar: {
				      columnWidth: "20%"
				    }
			  	},
			  	xaxis: {
			  		labels: {
			  			style: {
			  				colors: "red",
			  				fontWeight: 900
			  			}
			  		},
			    	categories: ['< 07:00', '07:00 - 08:00', '08:00 - 09:00', '09:00 - 10:00', '10:00 - 11:00', '11:00 - 12:00', '12:00 - 13:00', '13:00 - 14:00', '14:00 - 15:00', '15:00 - 16:00', '16:00 - 17:00', '17:00 - 18:00', '18:00 - 19:00', '19:00 - 20:00', '20:00 - 21:00', '21:00 - 22:00', '> 22:00'],
			    	title: {
			        	text: "จำนวนคนไข้ต่อชั่วโมง"
			      	}
			  	},
			  	yaxis: [
			    {
			    	opposite: false,
				    axisTicks: {
				        show: true
			      	},
			      	axisBorder: {
			        	show: true,
			      	},
			      	title: {
			        	text: "จำนวนคนไข้"
			      	},
			    }
			    ],
			  	tooltip: {
				    shared: false,
				    intersect: true,
				    x: {
				      	show: false
				    }
			  	},
			  	legend: {
				    horizontalAlign: "left",
				    offsetX: 40
			  	}
			};

			var chart = new ApexCharts(
				document.querySelector(Element),
				options
			);

			chart.render();
		}

		function DrawHorizontalBarChart(Element,Title,Data){

			var options = {
				chart: {
					height: 350,
					type: 'bar',
				},
				plotOptions: {
					bar: {
						horizontal: true,
						dataLabels: {
							position: 'top',
						},
					}  
				},
				dataLabels: {
					enabled: true,
					offsetX: -6,
					style: {
						fontSize: '12px',
						colors: [COLOR_WHITE]
					}
				},
				colors: [COLOR_ORANGE, COLOR_DARK],
				stroke: {
					show: true,
					width: 1,
					colors: [COLOR_WHITE]
				},
				series: [{
					name: 'ค่าเฉลี่ย (นาที)',
					data: Data
					}],
				xaxis: {
					categories: Title,
					axisBorder: {
						show: true,
						color: COLOR_SILVER_TRANSPARENT_5,
						height: 1,
						width: '100%',
						offsetX: 0,
						offsetY: -1
					},
					axisTicks: {
						show: true,
						borderType: 'solid',
						color: COLOR_SILVER,
						height: 6,
						offsetX: 0,
						offsetY: 0
					}
				}
			};
			
			var chart = new ApexCharts(
				document.querySelector(Element),
				options
			);

			chart.render();
		}

		function DrawLineAverageDaysChart(Element){
			// console.log(maleAll.replace(/"/g, '\''));
			var options = {
			  	chart: {
				    height: 385,
				    type: "line",
				    stacked: false
				},
			  	dataLabels: {
			    	enabled: false
			  	},
			  	colors: ['orange'],
			  	series: [
			    	/*{
				      	name: 'Male',
				      	type: 'line',
				      	data: maleAll
			    	},
			    	{
				      	name: "Female",
				      	type: 'line',
				      	data: femaleAll
			    	},*/
			    	{
				      	name: "Total",
				      	type: 'line',
				      	data: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
			    	}
			  	],
			  	stroke: {
			    	width: [4],
				    colors: [COLOR_ORANGE]
			  	},
			  	plotOptions: {
				    bar: {
				      columnWidth: "20%"
				    }
			  	},
			  	xaxis: {
			  		labels: {
			  			style: {
			  				colors: "red",
			  				fontWeight: 900
			  			}
			  		},
			    	categories: ['< 07:00', '07:00 - 08:00', '08:00 - 09:00', '09:00 - 10:00', '10:00 - 11:00', '11:00 - 12:00', '12:00 - 13:00', '13:00 - 14:00', '14:00 - 15:00', '15:00 - 16:00', '16:00 - 17:00', '17:00 - 18:00', '18:00 - 19:00', '19:00 - 20:00', '20:00 - 21:00', '21:00 - 22:00', '> 22:00'],
			    	title: {
			        	text: "จำนวนคนไข้ต่อชั่วโมง"
			      	}
			  	},
			  	yaxis: [
			    {
			    	opposite: false,
				    axisTicks: {
				        show: true
			      	},
			      	axisBorder: {
			        	show: true,
			      	},
			      	title: {
			        	text: "จำนวนคนไข้"
			      	},
			      	max:20,
			    }
			    ],
			  	tooltip: {
				    shared: false,
				    intersect: true,
				    x: {
				      	show: false
				    }
			  	},
			  	legend: {
				    horizontalAlign: "left",
				    offsetX: 40
			  	}
			};

			var chart = new ApexCharts(
				document.querySelector(Element),
				options
			);

			chart.render();
		}

		function DrawLineAverageMonthsChart(Element){
			// console.log(maleAll.replace(/"/g, '\''));
			var options = {
			  	chart: {
				    height: 385,
				    type: "line",
				    stacked: false
				},
			  	dataLabels: {
			    	enabled: false
			  	},
			  	colors: ['orange'],
			  	series: [
			    	/*{
				      	name: 'Male',
				      	type: 'line',
				      	data: maleAll
			    	},
			    	{
				      	name: "Female",
				      	type: 'line',
				      	data: femaleAll
			    	},*/
			    	{
				      	name: "Total",
				      	type: 'line',
				      	data: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
			    	}
			  	],
			  	stroke: {
			    	width: [4],
				    colors: [COLOR_ORANGE]
			  	},
			  	plotOptions: {
				    bar: {
				      columnWidth: "20%"
				    }
			  	},
			  	xaxis: {
			  		labels: {
			  			style: {
			  				colors: "red",
			  				fontWeight: 900
			  			}
			  		},
			    	categories: ['< 07:00', '07:00 - 08:00', '08:00 - 09:00', '09:00 - 10:00', '10:00 - 11:00', '11:00 - 12:00', '12:00 - 13:00', '13:00 - 14:00', '14:00 - 15:00', '15:00 - 16:00', '16:00 - 17:00', '17:00 - 18:00', '18:00 - 19:00', '19:00 - 20:00', '20:00 - 21:00', '21:00 - 22:00', '> 22:00'],
			    	title: {
			        	text: "จำนวนคนไข้ต่อชั่วโมง"
			      	}
			  	},
			  	yaxis: [
			    {
			    	opposite: false,
				    axisTicks: {
				        show: true
			      	},
			      	axisBorder: {
			        	show: true,
			      	},
			      	title: {
			        	text: "จำนวนคนไข้"
			      	},
			      	max:20,
			    }
			    ],
			  	tooltip: {
				    shared: false,
				    intersect: true,
				    x: {
				      	show: false
				    }
			  	},
			  	legend: {
				    horizontalAlign: "left",
				    offsetX: 40
			  	}
			};

			var chart = new ApexCharts(
				document.querySelector(Element),
				options
			);

			chart.render();
		}

		function DrawLineAverageYearsChart(Element){
			// console.log(maleAll.replace(/"/g, '\''));
			var options = {
			  	chart: {
				    height: 385,
				    type: "line",
				    stacked: false
				},
			  	dataLabels: {
			    	enabled: false
			  	},
			  	colors: ['orange'],
			  	series: [
			    	/*{
				      	name: 'Male',
				      	type: 'line',
				      	data: maleAll
			    	},
			    	{
				      	name: "Female",
				      	type: 'line',
				      	data: femaleAll
			    	},*/
			    	{
				      	name: "Total",
				      	type: 'line',
				      	data: [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17]
			    	}
			  	],
			  	stroke: {
			    	width: [4],
				    colors: [COLOR_ORANGE]
			  	},
			  	plotOptions: {
				    bar: {
				      columnWidth: "20%"
				    }
			  	},
			  	xaxis: {
			  		labels: {
			  			style: {
			  				colors: "red",
			  				fontWeight: 900
			  			}
			  		},
			    	categories: ['< 07:00', '07:00 - 08:00', '08:00 - 09:00', '09:00 - 10:00', '10:00 - 11:00', '11:00 - 12:00', '12:00 - 13:00', '13:00 - 14:00', '14:00 - 15:00', '15:00 - 16:00', '16:00 - 17:00', '17:00 - 18:00', '18:00 - 19:00', '19:00 - 20:00', '20:00 - 21:00', '21:00 - 22:00', '> 22:00'],
			    	title: {
			        	text: "จำนวนคนไข้ต่อชั่วโมง"
			      	}
			  	},
			  	yaxis: [
			    {
			    	opposite: false,
				    axisTicks: {
				        show: true
			      	},
			      	axisBorder: {
			        	show: true,
			      	},
			      	title: {
			        	text: "จำนวนคนไข้"
			      	},
			      	max:20,
			    }
			    ],
			  	tooltip: {
				    shared: false,
				    intersect: true,
				    x: {
				      	show: false
				    }
			  	},
			  	legend: {
				    horizontalAlign: "left",
				    offsetX: 40
			  	}
			};

			var chart = new ApexCharts(
				document.querySelector(Element),
				options
			);

			chart.render();
		}


		$( document ).ready(function() {
			if( "<?=$this->session->flashdata('logResult');?>" != ""){
				$.notify(
					"<?=$this->session->flashdata('logResult');?>",
					{position:"top center",className:"success"}
				);				
			}

			$.get( '<?=base_url('Admin/DataControl/CurrentProcessTime_Graph');?>', function( data ) {
				result = JSON.parse(data);
				
				DrawHorizontalBarChart('#groupPatientProcess-chart',result['careprovidername'],result['sum_time_minute']);
			});
			/*
			DrawLineAverageDaysChart('#Averagedays-chart');
			DrawLineAverageMonthsChart('#Averagemonths-chart');
			DrawLineAverageYearsChart('#Averageyears-chart');
			*/
			$.get( '<?=base_url('Admin/DataControl/TotalPatientLineGraph_Interval_Day/0');?>', function( data ) {
				result = JSON.parse(data);
				if(result != 'No Data'){
					
					
					var patientPerHours = [];
					patientPerHours[0] = result.Total1;
					patientPerHours[1] = result.Total2;
					patientPerHours[2] = result.Total3;
					patientPerHours[3] = result.Total4;
					patientPerHours[4] = result.Total5;
					patientPerHours[5] = result.Total6;
					patientPerHours[6] = result.Total7;
					patientPerHours[7] = result.Total8;
					patientPerHours[8] = result.Total9;
					patientPerHours[9] = result.Total10;
					patientPerHours[10] = result.Total11;
					patientPerHours[11] = result.Total12;
					patientPerHours[12] = result.Total13;
					patientPerHours[13] = result.Total14;
					patientPerHours[14] = result.Total15;
					patientPerHours[15] = result.Total16;
					patientPerHours[16] = result.Total17;

					//DrawLineChart('#Averageyears',maleAll,femaleAll,patientPerHours);
					$('#Averagedays-chart').html('');
					DrawLineChart('#Averagedays-chart',patientPerHours);
				}else{
					textRes = "ไม่มีข้อมูลสำหรับวันนี้";
					$('#Averagedays-chart').text(textRes);
				}
			});

			$.get( '<?=base_url('Admin/DataControl/TotalPatientLineGraph_Interval_Month/0');?>', function( data ) {
				result = JSON.parse(data);
				if(result != 'No Data'){
					
					var patientPerHours = [];
					patientPerHours[0] = result.Total1;
					patientPerHours[1] = result.Total2;
					patientPerHours[2] = result.Total3;
					patientPerHours[3] = result.Total4;
					patientPerHours[4] = result.Total5;
					patientPerHours[5] = result.Total6;
					patientPerHours[6] = result.Total7;
					patientPerHours[7] = result.Total8;
					patientPerHours[8] = result.Total9;
					patientPerHours[9] = result.Total10;
					patientPerHours[10] = result.Total11;
					patientPerHours[11] = result.Total12;
					patientPerHours[12] = result.Total13;
					patientPerHours[13] = result.Total14;
					patientPerHours[14] = result.Total15;
					patientPerHours[15] = result.Total16;
					patientPerHours[16] = result.Total17;

					//DrawLineChart('#Averageyears',maleAll,femaleAll,patientPerHours);
					$('#Averagemonths-chart').html('');
					DrawLineChart('#Averagemonths-chart',patientPerHours);
				}else{
					textRes = "ไม่มีข้อมูลสำหรับวันนี้";
					$('#Averagemonths-chart').text(textRes);
				}
			});

			$.get( '<?=base_url('Admin/DataControl/TotalPatientLineGraph_Interval_Year/0');?>', function( data ) {
				result = JSON.parse(data);
				if(result != 'No Data'){
					
					
					var patientPerHours = [];
					patientPerHours[0] = result.Total1;
					patientPerHours[1] = result.Total2;
					patientPerHours[2] = result.Total3;
					patientPerHours[3] = result.Total4;
					patientPerHours[4] = result.Total5;
					patientPerHours[5] = result.Total6;
					patientPerHours[6] = result.Total7;
					patientPerHours[7] = result.Total8;
					patientPerHours[8] = result.Total9;
					patientPerHours[9] = result.Total10;
					patientPerHours[10] = result.Total11;
					patientPerHours[11] = result.Total12;
					patientPerHours[12] = result.Total13;
					patientPerHours[13] = result.Total14;
					patientPerHours[14] = result.Total15;
					patientPerHours[15] = result.Total16;
					patientPerHours[16] = result.Total17;

					//DrawLineChart('#Averageyears',maleAll,femaleAll,patientPerHours);
					$('#Averageyears-chart').html('');
					DrawLineChart('#Averageyears-chart',patientPerHours);
				}else{
					textRes = "ไม่มีข้อมูลสำหรับวันนี้";
					$('#Averageyears-chart').text(textRes);
				}
			});
			
			//Graph Patient Number Interval
			var IntervalDays = 7;
			$.get( '<?=base_url('Admin/DataControl/LocalTotalPatient_Graph/');?>'+IntervalDays, function( data ) {
				result = JSON.parse(data);
				$('#TitleChart01').html("จำนวนคนไข้ทั้งหมดในรอบ " + IntervalDays + " วัน");
				DrawBarChart('#apex-mixed-chart',IntervalDays,result['Resultdate'],result['walkin'],result['appoint'],result['Sum_WnA']);
			});

			$.get( '<?=base_url('Admin/DataControl/LocateTotalPatientAll_Interval/');?>'+0, function( data ) {
				result = JSON.parse(data);
				
				var ChartTitle = "Total Patient " + result['Sum'];
				DrawPieChart('#LocateTotalPatient-chart',ChartTitle,result['walkin'],result['appoint']);
			});

			$.get( '<?=base_url('Admin/DataControl/TotalPatientLineGraph/');?>', function( data ) {
				result = JSON.parse(data);
				if(result != 'No Data'){
					
					var maleAll = [];
					maleAll[0] = result.Male1;
					maleAll[1] = result.Male2;
					maleAll[2] = result.Male3;
					maleAll[3] = result.Male4;
					maleAll[4] = result.Male5;
					maleAll[5] = result.Male6;
					maleAll[6] = result.Male7;
					maleAll[7] = result.Male8;
					maleAll[8] = result.Male9;
					maleAll[9] = result.Male10;
					maleAll[10] = result.Male11;
					maleAll[11] = result.Male12;
					maleAll[12] = result.Male13;
					maleAll[13] = result.Male14;
					maleAll[14] = result.Male15;
					maleAll[15] = result.Male16;
					maleAll[16] = result.Male17;

					var femaleAll = [];
					femaleAll[0] = result.Female1;
					femaleAll[1] = result.Female2;
					femaleAll[2] = result.Female3;
					femaleAll[3] = result.Female4;
					femaleAll[4] = result.Female5;
					femaleAll[5] = result.Female6;
					femaleAll[6] = result.Female7;
					femaleAll[7] = result.Female8;
					femaleAll[8] = result.Female9;
					femaleAll[9] = result.Female10;
					femaleAll[10] = result.Female11;
					femaleAll[11] = result.Female12;
					femaleAll[12] = result.Female13;
					femaleAll[13] = result.Female14;
					femaleAll[14] = result.Female15;
					femaleAll[15] = result.Female16;
					femaleAll[16] = result.Female17;

					var patientPerHours = [];
					patientPerHours[0] = result.Total1;
					patientPerHours[1] = result.Total2;
					patientPerHours[2] = result.Total3;
					patientPerHours[3] = result.Total4;
					patientPerHours[4] = result.Total5;
					patientPerHours[5] = result.Total6;
					patientPerHours[6] = result.Total7;
					patientPerHours[7] = result.Total8;
					patientPerHours[8] = result.Total9;
					patientPerHours[9] = result.Total10;
					patientPerHours[10] = result.Total11;
					patientPerHours[11] = result.Total12;
					patientPerHours[12] = result.Total13;
					patientPerHours[13] = result.Total14;
					patientPerHours[14] = result.Total15;
					patientPerHours[15] = result.Total16;
					patientPerHours[16] = result.Total17;

					//DrawLineChart('#LinechartPerHours',maleAll,femaleAll,patientPerHours);
					DrawLineChart('#LinechartPerHours',patientPerHours);
				}else{
					textRes = "ไม่มีข้อมูลสำหรับวันนี้";
					$('#LinechartPerHours').text(textRes);
				}
			});
			
			$("#LocalTotalPatient-Panel > .js-slider-home-prev").click(function() {
				console.log("PREV");
				IntervalDays -= 1;
				$.get( '<?=base_url('Admin/DataControl/LocalTotalPatient_Graph/');?>'+IntervalDays, function( data ) {
					result = JSON.parse(data);
					$('#TitleChart01').html("จำนวนคนไข้ทั้งหมดในรอบ " + IntervalDays + " วัน");
					$('#apex-mixed-chart').html("");
					DrawBarChart('#apex-mixed-chart',IntervalDays,result['Resultdate'],result['walkin'],result['appoint'],result['Sum_WnA']);
				});
			});

			$("#LocalTotalPatient-Panel > .js-slider-home-next").click(function() {
				console.log("NEXT");
				IntervalDays += 1;
				$.get( '<?=base_url('Admin/DataControl/LocalTotalPatient_Graph/');?>'+IntervalDays, function( data ) {
					result = JSON.parse(data);
					$('#TitleChart01').html("จำนวนคนไข้ทั้งหมดในรอบ " + IntervalDays + " วัน");
					$('#apex-mixed-chart').html("");
					DrawBarChart('#apex-mixed-chart',IntervalDays,result['Resultdate'],result['walkin'],result['appoint'],result['Sum_WnA']);
				});

			});

			var IntervalGroupPatientProcess = 0;
			$("#groupPatientProcess-Panel > button").click(function() {
				Control = $(this).data("control");
				switch (Control){
					case 'Prev':
						console.log("PREV");
						IntervalGroupPatientProcess += 1;
						break;
					case 'Next':
						console.log("NEXT");
						IntervalGroupPatientProcess -= 1;
						break;
				}
				if(IntervalGroupPatientProcess < 0){ IntervalGroupPatientProcess = 0;}
				$.get( '<?=base_url('Admin/DataControl/CurrentProcessTime_Graph_Interval/');?>'+IntervalGroupPatientProcess, function( data ) {
					result = JSON.parse(data);
					if(IntervalGroupPatientProcess == 0){
						PanelTitle = "เวลาเฉลี่ย ของแผนก <?=$this->session->userdata('ses_locationtitle');?>";
					}
					else{
						PanelTitle = "เวลาเฉลี่ย ของแผนก <?=$this->session->userdata('ses_locationtitle');?> ของ "+IntervalGroupPatientProcess+ " วันที่ผ่านมา";
					}
					$('#groupPatientProcess-title').html(PanelTitle);
					$('#groupPatientProcess-chart').html('');
					DrawHorizontalBarChart('#groupPatientProcess-chart',result['careprovidername'],result['sum_time_minute']);
				});		
			});

			var IntervalLocateTotalPatientAll = 0;
			$("#LocateTotalPatient-Panel > button").click(function() {
				Control = $(this).data("control");
				switch (Control){
					case 'Prev':
						console.log("PREV");
						IntervalLocateTotalPatientAll += 1;
						break;
					case 'Next':
						console.log("NEXT");
						IntervalLocateTotalPatientAll -= 1;
						break;
				}
				if(IntervalLocateTotalPatientAll < 0){ IntervalLocateTotalPatientAll = 0;}
				$.get( '<?=base_url('Admin/DataControl/LocateTotalPatientAll_Interval/');?>'+IntervalLocateTotalPatientAll, function( data ) {
					result = JSON.parse(data);
					var ChartTitle = "Total Patient " + result['Sum'];
					if(IntervalLocateTotalPatientAll == 0){
						PanelTitle = "จำนวนคนไข้ ของแผนก <?=$this->session->userdata('ses_locationtitle');?>";
					}
					else{
						PanelTitle = "จำนวนคนไข้ ของแผนก <?=$this->session->userdata('ses_locationtitle');?> ของ "+IntervalLocateTotalPatientAll+ " วันที่ผ่านมา";
					}
					$('#LocateTotalPatient-title').html(PanelTitle);
					$('#LocateTotalPatient-chart').html('');
					DrawPieChart('#LocateTotalPatient-chart',ChartTitle,result['walkin'],result['appoint']);
				});
			});

			var IntervalLinechartPerHours = 0;
			$("#LinechartPerHours-Panel > button").click(function() {
				Control = $(this).data("control");
				switch (Control){
					case 'Prev':
						console.log("PREV");
						IntervalLinechartPerHours += 1;
						break;
					case 'Next':
						console.log("NEXT");
						IntervalLinechartPerHours -= 1;
						break;
				}
				if(IntervalLinechartPerHours < 0){ IntervalLinechartPerHours = 0;}
				$.get( '<?=base_url('Admin/DataControl/TotalPatientLineGraph_Interval/');?>'+IntervalLinechartPerHours, function( data ) {
					result = JSON.parse(data);
					if(result != 'No Data'){
						
						var maleAll = [];
						maleAll[0] = result.Male1;
						maleAll[1] = result.Male2;
						maleAll[2] = result.Male3;
						maleAll[3] = result.Male4;
						maleAll[4] = result.Male5;
						maleAll[5] = result.Male6;
						maleAll[6] = result.Male7;
						maleAll[7] = result.Male8;
						maleAll[8] = result.Male9;
						maleAll[9] = result.Male10;
						maleAll[10] = result.Male11;
						maleAll[11] = result.Male12;
						maleAll[12] = result.Male13;
						maleAll[13] = result.Male14;
						maleAll[14] = result.Male15;
						maleAll[15] = result.Male16;
						maleAll[16] = result.Male17;

						var femaleAll = [];
						femaleAll[0] = result.Female1;
						femaleAll[1] = result.Female2;
						femaleAll[2] = result.Female3;
						femaleAll[3] = result.Female4;
						femaleAll[4] = result.Female5;
						femaleAll[5] = result.Female6;
						femaleAll[6] = result.Female7;
						femaleAll[7] = result.Female8;
						femaleAll[8] = result.Female9;
						femaleAll[9] = result.Female10;
						femaleAll[10] = result.Female11;
						femaleAll[11] = result.Female12;
						femaleAll[12] = result.Female13;
						femaleAll[13] = result.Female14;
						femaleAll[14] = result.Female15;
						femaleAll[15] = result.Female16;
						femaleAll[16] = result.Female17;

						var patientPerHours = [];
						patientPerHours[0] = result.Total1;
						patientPerHours[1] = result.Total2;
						patientPerHours[2] = result.Total3;
						patientPerHours[3] = result.Total4;
						patientPerHours[4] = result.Total5;
						patientPerHours[5] = result.Total6;
						patientPerHours[6] = result.Total7;
						patientPerHours[7] = result.Total8;
						patientPerHours[8] = result.Total9;
						patientPerHours[9] = result.Total10;
						patientPerHours[10] = result.Total11;
						patientPerHours[11] = result.Total12;
						patientPerHours[12] = result.Total13;
						patientPerHours[13] = result.Total14;
						patientPerHours[14] = result.Total15;
						patientPerHours[15] = result.Total16;
						patientPerHours[16] = result.Total17;

						//DrawLineChart('#LinechartPerHours',maleAll,femaleAll,patientPerHours);
						$('#LinechartPerHours').html('');
						DrawLineChart('#LinechartPerHours',patientPerHours);
					}else{
						textRes = "ไม่มีข้อมูลสำหรับวันนี้";
						$('#LinechartPerHours').text(textRes);
					}

					if(IntervalLinechartPerHours == 0){
						PanelTitle = "จำนวนคนไข้ ของแผนก <?=$this->session->userdata('ses_locationtitle');?>";
					}
					else{
						PanelTitle = "จำนวนคนไข้ ของแผนก <?=$this->session->userdata('ses_locationtitle');?> ของ "+IntervalLinechartPerHours+ " วันที่ผ่านมา";
					}
					$('#LinechartPerHours-title').html(PanelTitle);
				});
			});

		});

			var IntervalAveragedays = 0;
			$("#Averagedays-Panel > button").click(function() {
				Control = $(this).data("control");
				switch (Control){
					case 'Prev':
						console.log("PREV");
						IntervalAveragedays += 1;
						break;
					case 'Next':
						console.log("NEXT");
						IntervalAveragedays -= 1;
						break;
				}
				if(IntervalAveragedays < 0){ IntervalAveragedays = 0;}
				$.get( '<?=base_url('Admin/DataControl/TotalPatientLineGraph_Interval_Day/');?>'+IntervalAveragedays, function( data ) {
					result = JSON.parse(data);
					if(result != 'No Data'){
												
						var patientPerHours = [];
						patientPerHours[0] = result.Total1;
						patientPerHours[1] = result.Total2;
						patientPerHours[2] = result.Total3;
						patientPerHours[3] = result.Total4;
						patientPerHours[4] = result.Total5;
						patientPerHours[5] = result.Total6;
						patientPerHours[6] = result.Total7;
						patientPerHours[7] = result.Total8;
						patientPerHours[8] = result.Total9;
						patientPerHours[9] = result.Total10;
						patientPerHours[10] = result.Total11;
						patientPerHours[11] = result.Total12;
						patientPerHours[12] = result.Total13;
						patientPerHours[13] = result.Total14;
						patientPerHours[14] = result.Total15;
						patientPerHours[15] = result.Total16;
						patientPerHours[16] = result.Total17;

						//DrawLineChart('#Averagedays',maleAll,femaleAll,patientPerHours);
						$('#Averagedays-chart').html('');
						DrawLineChart('#Averagedays-chart',patientPerHours);
					}else{
						textRes = "ไม่มีข้อมูลสำหรับวันนี้";
						$('#Averagedays-chart').text(textRes);
					}

					if(IntervalAveragedays == 0){
						PanelTitle = "เฉลี่ยรายวัน ของแผนก <?=$this->session->userdata('ses_locationtitle');?>";
					}
					else{
						PanelTitle = "เฉลี่ยรายวัน ของแผนก <?=$this->session->userdata('ses_locationtitle');?> ของ "+IntervalAveragedays+ " วันที่ผ่านมา";
					}
					$('#Averagedays-title').html(PanelTitle);
				});
			});

			var IntervalAveragemonths = 0;
			$("#Averagemonths-Panel > button").click(function() {
				Control = $(this).data("control");
				switch (Control){
					case 'Prev':
						console.log("PREV");
						IntervalAveragemonths += 1;
						break;
					case 'Next':
						console.log("NEXT");
						IntervalAveragemonths -= 1;
						break;
				}
				if(IntervalAveragemonths < 0){ IntervalAveragemonths = 0;}
				$.get( '<?=base_url('Admin/DataControl/TotalPatientLineGraph_Interval_Month/');?>'+IntervalAveragemonths, function( data ) {
					result = JSON.parse(data);
					if(result != 'No Data'){

						var patientPerHours = [];
						patientPerHours[0] = result.Total1;
						patientPerHours[1] = result.Total2;
						patientPerHours[2] = result.Total3;
						patientPerHours[3] = result.Total4;
						patientPerHours[4] = result.Total5;
						patientPerHours[5] = result.Total6;
						patientPerHours[6] = result.Total7;
						patientPerHours[7] = result.Total8;
						patientPerHours[8] = result.Total9;
						patientPerHours[9] = result.Total10;
						patientPerHours[10] = result.Total11;
						patientPerHours[11] = result.Total12;
						patientPerHours[12] = result.Total13;
						patientPerHours[13] = result.Total14;
						patientPerHours[14] = result.Total15;
						patientPerHours[15] = result.Total16;
						patientPerHours[16] = result.Total17;

						//DrawLineChart('#Averagemonths',maleAll,femaleAll,patientPerHours);
						$('#Averagemonths-chart').html('');
						DrawLineChart('#Averagemonths-chart',patientPerHours);
					}else{
						textRes = "ไม่มีข้อมูลสำหรับช่วงเดือนนี้";
						$('#Averagemonths-chart').text(textRes);
					}

					if(IntervalAveragemonths == 0){
						PanelTitle = "เฉลี่ยรายเดือน ของแผนก <?=$this->session->userdata('ses_locationtitle');?>";
					}
					else{
						PanelTitle = "เฉลี่ยรายเดือน ของแผนก <?=$this->session->userdata('ses_locationtitle');?> ของรอบ "+IntervalAveragemonths+ " เดือนที่ผ่านมา";
					}
					$('#Averagemonths-title').html(PanelTitle);
				});
			});
			
			var IntervalAverageyears = 0;
			$("#Averageyears-Panel > button").click(function() {
				Control = $(this).data("control");
				switch (Control){
					case 'Prev':
						console.log("PREV");
						IntervalAverageyears += 1;
						break;
					case 'Next':
						console.log("NEXT");
						IntervalAverageyears -= 1;
						break;
				}
				if(IntervalAverageyears < 0){ IntervalAverageyears = 0;}
				$.get( '<?=base_url('Admin/DataControl/TotalPatientLineGraph_Interval_Year/');?>'+IntervalAverageyears, function( data ) {
					result = JSON.parse(data);
					if(result != 'No Data'){
						
						
						var patientPerHours = [];
						patientPerHours[0] = result.Total1;
						patientPerHours[1] = result.Total2;
						patientPerHours[2] = result.Total3;
						patientPerHours[3] = result.Total4;
						patientPerHours[4] = result.Total5;
						patientPerHours[5] = result.Total6;
						patientPerHours[6] = result.Total7;
						patientPerHours[7] = result.Total8;
						patientPerHours[8] = result.Total9;
						patientPerHours[9] = result.Total10;
						patientPerHours[10] = result.Total11;
						patientPerHours[11] = result.Total12;
						patientPerHours[12] = result.Total13;
						patientPerHours[13] = result.Total14;
						patientPerHours[14] = result.Total15;
						patientPerHours[15] = result.Total16;
						patientPerHours[16] = result.Total17;

						//DrawLineChart('#Averageyears',maleAll,femaleAll,patientPerHours);
						$('#Averageyears-chart').html('');
						DrawLineChart('#Averageyears-chart',patientPerHours);
					}else{
						textRes = "ไม่มีข้อมูลสำหรับช่วงปีนี้";
						$('#Averageyears-chart').text(textRes);
					}

					if(IntervalAverageyears == 0){
						PanelTitle = "เฉลี่ยรายปี ของแผนก <?=$this->session->userdata('ses_locationtitle');?>";
					}
					else{
						PanelTitle = "เฉลี่ยรายปี ของแผนก <?=$this->session->userdata('ses_locationtitle');?> ของรอบ "+IntervalAverageyears+ " ปีที่ผ่านมา";
					}
					$('#Averageyears-title').html(PanelTitle);
				});
			});


	</script>