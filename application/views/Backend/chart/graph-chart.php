
  <section class="slideshow" id="js-header">
    <div class="slideshow__slide js-slider-home-slide is-current" data-slide="1">
	  	<div class="slideshow__slide-background-parallax js-parallax" data-speed="-1" data-position="top" data-target="#js-header">
	        <div id="apex-mixed-chart"></div>
	  	</div>
	</div>
	<!--
	<div class="slideshow__slide js-slider-home-slide is-next" data-slide="2">
	  	<div class="slideshow__slide-background-parallax js-parallax" data-speed="-1" data-position="top" data-target="#js-header">
	        <div id="apex-mixed-chart"></div>
	  	</div>
	</div>
    -->
    <div class="c-header-home_footer">
		<div class="o-container">
			<div class="c-header-home_controls -nomobile o-button-group">
				<div class="js-parallax is-inview" data-speed="1" data-position="top" data-target="#js-header">
					<button class="o-button -white -square -left js-slider-home-button js-slider-home-prev" type="button">
						<span class="o-button_label">
							<i class="fas fa-long-arrow-alt-left next_sty"></i>
						</span>
					</button>
					<button class="o-button -white -square js-slider-home-button js-slider-home-next -right" type="button">
						<span class="o-button_label">
							<i class="fas fa-long-arrow-alt-right next_sty"></i>
						</span>
					</button>
				</div>
			</div>
		</div>
	</div>
   
    
  </section>
  
<!-- <svg xmlns="http://www.w3.org/2000/svg">
    <symbol viewBox="0 0 18 18" id="arrow-next">
        <path id="arrow-next-arrow.svg" d="M12.6,9L4,17.3L4.7,18l8.5-8.3l0,0L14,9l0,0l-0.7-0.7l0,0L4.7,0L4,0.7L12.6,9z"/>
    </symbol>
    <symbol viewBox="0 0 18 18" id="arrow-prev">
        <path id="arrow-prev-arrow.svg" d="M14,0.7L13.3,0L4.7,8.3l0,0L4,9l0,0l0.7,0.7l0,0l8.5,8.3l0.7-0.7L5.4,9L14,0.7z"/>
    </symbol>
</svg> -->