<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Report Rayong Dashboard</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<?php 
		echo assets_css('fonts/kanit/stylesheet.css');
		echo assets_css('app.min.css');
		echo assets_css('bootstrap-datepicker.css');
		echo assets_css('chartjs/slide-chartcss.css');
	?>

	<?php /*echo assets_css('jquery.gritter.css')*/;?>
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		
		<?php $this->view($headercontent); ?>

		<?php $this->view($sidebarcontent); ?>
		
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">จำนวนคนไข้รายวันแบบกราฟ</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Dashboard</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
				<!-- begin col-6 -->
				<div class="col-xl-12">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="index-7">
						<div class="panel-heading">
							<h4 class="panel-title">จำนวนคนไข้ ของแผนก <?=$this->session->userdata('ses_locationtitle');?></h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div class="col-lg-6">
								<div class="input-group-append input-daterange">							<input type="text" class="form-control" name="Days" id="datepicker-default" placeholder="เลือกวันที่" data-date-format='yyyy-mm-dd' />
									<button tabindex="100" class="btn btn-md btn-primary" id="DaysChoose" type="button">Submit</button>
								</div>
							</div>
							<div id="LinechartPerHours"></div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-12 -->
			</div>
		</div>
		<!-- end #content -->
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	<?php
	echo assets_js('app.min.js');
	echo assets_js('default.min.js');
	echo assets_js('form-plugins.demo.js');
	// echo assets_js('jquery.gritter.js');
	echo assets_js('bootstrap-datepicker.js');
	echo assets_js('notify.min.js');
	echo assets_js('dashboard.js');
	echo assets_js('chartjs/chart-apex.demo.js');
	echo assets_js('chartjs/graph-charts.min.js');
	echo assets_js('chartjs/slide-chartjs.js');
	?>
	<script type="text/javascript" charset="utf-8">
		$( document ).ready(function() {
			if( "<?=$this->session->flashdata('logResult');?>" != ""){
				$.notify(
					"<?=$this->session->flashdata('logResult');?>",
					{position:"top center",className:"success"}
				);				
			}

			$('#DaysChoose').click(function(){
				var days = $('input[name=Days]').val();
				$('#LinechartPerHours').empty();
				$.get( '<?=base_url('Admin/DataControl/TotalLinePatientPerHoursWithDays/');?>'+days, function( data ) {
					result = JSON.parse(data);
					if(result != 'No Data'){
						/*console.log(result);
						var maleAll = [];
						maleAll[0] = result.Male1;
						maleAll[1] = result.Male2;
						maleAll[2] = result.Male3;
						maleAll[3] = result.Male4;
						maleAll[4] = result.Male5;
						maleAll[5] = result.Male6;
						maleAll[6] = result.Male7;
						maleAll[7] = result.Male8;
						maleAll[8] = result.Male9;
						maleAll[9] = result.Male10;
						maleAll[10] = result.Male11;
						maleAll[11] = result.Male12;
						maleAll[12] = result.Male13;
						maleAll[13] = result.Male14;
						maleAll[14] = result.Male15;
						maleAll[15] = result.Male16;
						maleAll[16] = result.Male17;

						var femaleAll = [];
						femaleAll[0] = result.Female1;
						femaleAll[1] = result.Female2;
						femaleAll[2] = result.Female3;
						femaleAll[3] = result.Female4;
						femaleAll[4] = result.Female5;
						femaleAll[5] = result.Female6;
						femaleAll[6] = result.Female7;
						femaleAll[7] = result.Female8;
						femaleAll[8] = result.Female9;
						femaleAll[9] = result.Female10;
						femaleAll[10] = result.Female11;
						femaleAll[11] = result.Female12;
						femaleAll[12] = result.Female13;
						femaleAll[13] = result.Female14;
						femaleAll[14] = result.Female15;
						femaleAll[15] = result.Female16;
						femaleAll[16] = result.Female17;*/

						var patientPerHours = [];
						patientPerHours[0] = result.Total1;
						patientPerHours[1] = result.Total2;
						patientPerHours[2] = result.Total3;
						patientPerHours[3] = result.Total4;
						patientPerHours[4] = result.Total5;
						patientPerHours[5] = result.Total6;
						patientPerHours[6] = result.Total7;
						patientPerHours[7] = result.Total8;
						patientPerHours[8] = result.Total9;
						patientPerHours[9] = result.Total10;
						patientPerHours[10] = result.Total11;
						patientPerHours[11] = result.Total12;
						patientPerHours[12] = result.Total13;
						patientPerHours[13] = result.Total14;
						patientPerHours[14] = result.Total15;
						patientPerHours[15] = result.Total16;
						patientPerHours[16] = result.Total17;

						console.log(patientPerHours);

						DrawLineChart('#LinechartPerHours',patientPerHours);
					}else{
						textRes = "ไม่มีข้อมูลสำหรับวันนี้";
						$('#LinechartPerHours').text(textRes);
					}
				});
			});
			
			
		});

		function DrawLineChart(Element,patientPerHours){
			// console.log(maleAll.replace(/"/g, '\''));
			var options = {
			  	chart: {
				    height: 600,
				    type: "line",
				    stacked: false
				},
			  	dataLabels: {
			    	enabled: false
			  	},
			  	// colors: ['red', 'blue', 'orange'],
			  	colors: ['orange'],
			  	series: [
			    	// {
				    //   	name: 'Male',
				    //   	type: 'line',
				    //   	data: maleAll
			    	// },
			    	// {
				    //   	name: "Female",
				    //   	type: 'line',
				    //   	data: femaleAll
			    	// },
			    	{
				      	name: "Total",
				      	type: 'line',
				      	data: patientPerHours
			    	}
			  	],
			  	stroke: {
			    	// width: [4, 4, 4],
				    // colors: [COLOR_RED, COLOR_TEAL, COLOR_ORANGE]
			    	width: [4],
				    colors: [COLOR_ORANGE]
			  	},
			  	plotOptions: {
				    bar: {
				      columnWidth: "20%"
				    }
			  	},
			  	xaxis: {
			  		labels: {
			  			style: {
			  				colors: "red",
			  				fontWeight: 900
			  			}
			  		},
			    	categories: ['< 07:00', '07:00 - 08:00', '08:00 - 09:00', '09:00 - 10:00', '10:00 - 11:00', '11:00 - 12:00', '12:00 - 13:00', '13:00 - 14:00', '14:00 - 15:00', '15:00 - 16:00', '16:00 - 17:00', '17:00 - 18:00', '18:00 - 19:00', '19:00 - 20:00', '20:00 - 21:00', '21:00 - 22:00', '> 22:00'],
			    	title: {
			        	text: "จำนวนคนไข้ต่อชั่วโมง"
			      	}
			  	},
			  	yaxis: [
			    {
			    	opposite: false,
				    axisTicks: {
				        show: true
			      	},
			      	axisBorder: {
			        	show: true,
			      	},
			      	title: {
			        	text: "จำนวนคนไข้"
			      	},
			      	max:40
			    }
			    // {
			    //   	show: false,
			    //   	max:40
			    // },
			    // {
			    //   	show: false,
			    //   	max:40
			    // }
			  	],
			  	tooltip: {
				    shared: false,
				    intersect: true,
				    x: {
				      	show: false
				    }
			  	},
			  	legend: {
				    horizontalAlign: "left",
				    offsetX: 40
			  	}
			};

			var chart = new ApexCharts(
				document.querySelector(Element),
				options
			);

			chart.render();
		}

	</script>
</body>
</html>


