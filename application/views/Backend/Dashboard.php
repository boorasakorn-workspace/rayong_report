<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Report Rayong Dashboard</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<?php 
		echo assets_css('fonts/kanit/stylesheet.css');
		echo assets_css('app.min.css');
		echo assets_css('bootstrap-datepicker.css');
		echo assets_css('chartjs/slide-chartcss.css');
	?>

	<?php /*echo assets_css('jquery.gritter.css')*/;?>
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		
		<?php $this->view($headercontent); ?>

		<?php $this->view($sidebarcontent); ?>
		
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Dashboard</h1>
			<!-- end page-header -->

			<!-- begin row -->
			<div class="row">
				<!-- begin col-12 -->
				<div class="col-xl-12">					
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title" id="Averagedays-title">เฉลี่ยรายวัน <?=$this->session->userdata('ses_locationtitle');?></h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div id="Averagedays-chart"></div>
							<div id="Averagedays-Panel">
								<button class="o-button -white -square -left js-slider-home-button js-slider-home-prev defaultPrev" type="button" data-control="Prev">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-left next_sty"></i>
									</span>
								</button>
								<button class="o-button -white -square js-slider-home-button js-slider-home-next -right defaultNext" type="button" data-control="Next">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-right next_sty"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-12 -->
			</div>
			<!-- end row -->

			<!-- begin row -->
			<div class="row">
				<!-- begin col-12 -->
				<div class="col-xl-12">					
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title" id="Averagemonths-title">เฉลี่ยรายเดือน <?=$this->session->userdata('ses_locationtitle');?></h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div id="Averagemonths-chart"></div>
							<div id="Averagemonths-Panel">
								<button class="o-button -white -square -left js-slider-home-button js-slider-home-prev defaultPrev" type="button" data-control="Prev">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-left next_sty"></i>
									</span>
								</button>
								<button class="o-button -white -square js-slider-home-button js-slider-home-next -right defaultNext" type="button" data-control="Next">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-right next_sty"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-12 -->
			</div>
			<!-- end row -->

			<!-- begin row -->
			<div class="row">
				<!-- begin col-12 -->
				<div class="col-xl-12">					
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title" id="Averageyears-title">เฉลี่ยรายปี <?=$this->session->userdata('ses_locationtitle');?></h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div id="Averageyears-chart"></div>
							<div id="Averageyears-Panel">
								<button class="o-button -white -square -left js-slider-home-button js-slider-home-prev defaultPrev" type="button" data-control="Prev">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-left next_sty"></i>
									</span>
								</button>
								<button class="o-button -white -square js-slider-home-button js-slider-home-next -right defaultNext" type="button" data-control="Next">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-right next_sty"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-12 -->
			</div>
			<!-- end row -->

			<!-- begin row -->
			<div class="row">
				<!-- begin col-12 -->
				<div class="col-xl-12">					
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title" id="groupPatientProcess-title">เวลาเฉลี่ย ของแผนก <?=$this->session->userdata('ses_locationtitle');?></h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div id="groupPatientProcess-chart"></div>
							<div id="groupPatientProcess-Panel">
								<button class="o-button -white -square -left js-slider-home-button js-slider-home-prev defaultPrev" type="button" data-control="Prev">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-left next_sty"></i>
									</span>
								</button>
								<button class="o-button -white -square js-slider-home-button js-slider-home-next -right defaultNext" type="button" data-control="Next">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-right next_sty"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-12 -->
			</div>
			<!-- end row -->

			<!-- begin row -->
			<div class="row">
				<!-- begin col-6 -->
				<div class="col-xl-6">					
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title" id="LocateTotalPatient-title">จำนวนคนไข้ ของแผนก <?=$this->session->userdata('ses_locationtitle');?></h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div id="LocateTotalPatient-chart"></div>
							<div id="LocateTotalPatient-Panel">
								<button class="o-button -white -square -left js-slider-home-button js-slider-home-prev defaultPrev" type="button" data-control="Prev">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-left next_sty"></i>
									</span>
								</button>
								<button class="o-button -white -square js-slider-home-button js-slider-home-next -right defaultNext" type="button" data-control="Next">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-right next_sty"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-6 -->
				<!-- begin col-6 -->
				<div class="col-xl-6">
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="panel-title" id="TitleChart01">จำนวนคนไข้ทั้งหมด</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body pr-1">
							<!-- <div id="interactive-chart" class="height-sm"> -->
								<?php //$this->view($graphChart); ?>
							<!-- </div> -->
							<div id="apex-mixed-chart"></div>
							<div id="LocalTotalPatient-Panel">
								<button class="o-button -white -square -left js-slider-home-button js-slider-home-prev defaultPrev" type="button" data-control="Prev">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-left next_sty"></i>
									</span>
								</button>
								<button class="o-button -white -square js-slider-home-button js-slider-home-next -right defaultNext" type="button" data-control="Next">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-right next_sty"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-6 -->
			</div>
			<!-- begin row -->
			<div class="row">
				<!-- begin col-12 -->
				<div class="col-xl-12">
					<!-- begin panel -->
					<div class="panel panel-inverse">
						<div class="panel-heading">
							<h4 class="LinechartPerHours-title">จำนวนคนไข้ ของแผนก <?=$this->session->userdata('ses_locationtitle');?></h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<div id="LinechartPerHours"></div>
							<div id="LinechartPerHours-Panel">
								<button class="o-button -white -square -left js-slider-home-button js-slider-home-prev defaultPrev" type="button" data-control="Prev">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-left next_sty"></i>
									</span>
								</button>
								<button class="o-button -white -square js-slider-home-button js-slider-home-next -right defaultNext" type="button" data-control="Next">
									<span class="o-button_label">
										<i class="fas fa-long-arrow-alt-right next_sty"></i>
									</span>
								</button>
							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-6 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	<?php
	echo assets_js('app.min.js');
	echo assets_js('default.min.js');
	// echo assets_js('jquery.gritter.js');
	echo assets_js('bootstrap-datepicker.js');
	echo assets_js('notify.min.js');
	echo assets_js('dashboard.js');
	echo assets_js('chartjs/chart-apex.demo.js');
	echo assets_js('chartjs/graph-charts.min.js');
	echo assets_js('chartjs/slide-chartjs.js');
	$this->view($chart);
	?>
</body>
</html>


