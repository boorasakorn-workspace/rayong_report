<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Login | Report Rayong</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<?php echo assets_css('app.min.css'); ?>
	<!-- ================== END BASE CSS STYLE ================== -->
</head>
<body class="pace-top">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-v1">
			<!-- begin login-container -->
			<div class="login-container">
				<!-- begin login-header -->
				<div class="login-header">
					<div class="brand">
						<?=assets_img('S__11313218.png', array('width'=>'250px'));?> 
						<br/><b style="padding-left: 15px;color: #bb1623">Report</b> <span style="color: #003059; font-weight: 400;">DashQueue</span>
					</div>
					<div class="icon">
						<i class="fa fa-lock" style="background: #b48811; background: -moz-linear-gradient(top, #ebd197 0%, #b48811 50%, #a2790d 51%, #bb9b49 100%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ebd197), color-stop(50%,#b48811), color-stop(51%,#a2790d), color-stop(100%,#bb9b49)); background: -webkit-linear-gradient(top, #ebd197 0%,#b48811 50%,#a2790d 51%,#bb9b49 100%); background: -o-linear-gradient(top, #ebd197 0%,#b48811 50%,#a2790d 51%,#bb9b49 100%); background: -ms-linear-gradient(top, #ebd197 0%,#b48811 50%,#a2790d 51%,#bb9b49 100%); background: linear-gradient(to bottom, #ebd197 0%,#b48811 50%,#a2790d 51%,#bb9b49 100%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ebd197', endColorstr='#b48811',GradientType=0 );-webkit-background-clip: text;-webkit-text-fill-color: transparent;"></i>
					</div>
				</div>
				<!-- end login-header -->
				<!-- begin login-body -->
				<div class="login-body">
					<!-- begin login-content -->
					<div class="login-content">
						<form action="<?=base_url('Admin/Account/Login');?>" method="POST" class="margin-bottom-0">
							<div class="form-group m-b-20">
								<input type="text" name="inputUsername" id="inputUsername" class="form-control form-control-lg inverse-mode" placeholder="Username" required />
							</div>
							<div class="form-group m-b-20">
								<input type="password" name="inputPassword" id="inputPassword" class="form-control form-control-lg inverse-mode" placeholder="Password" required />
							</div>
							<div class="login-buttons">
								<button type="submit" class="btn btn-success btn-block btn-lg">Log On</button>
							</div>
						</form>
					</div>
					<!-- end login-content -->
				</div>
				<!-- end login-body -->
			</div>
			<!-- end login-container -->
		</div>
		<!-- end login -->
		
	</div>
	<!-- end page container -->
	<?php
	echo assets_js('app.min.js');
	echo assets_js('default.min.js');
	echo assets_js('notify.min.js');
	?>
	<script type="text/javascript" charset="utf-8">
		$( document ).ready(function() {
			if( "<?=$this->session->flashdata('logResult');?>" != ""){
				$.notify(
					"<?=$this->session->flashdata('logResult');?>",
					{position:"top center",className:"error"}
				);				
			}
		});

	</script>
</body>
</html>
