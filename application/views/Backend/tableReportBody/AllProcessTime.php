
							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th width="1%"></th>
											<th class="text-nowrap">ชื่อแพทย์</th>
											<th class="text-nowrap">Scan - Send to Doctor</th>

											<th class="text-nowrap">Send to Doctor - Med D/C</th>

											<?php /*

											<?php if($this->session->userdata('ses_applocation') != '3'){ ?>
											<th class="text-nowrap">Scan - Doctor Call</th>
											<?php } ?>
											<?php if($this->session->userdata('ses_applocation') == '3'){ ?>
												<th class="text-nowrap">Send to Doctor - Med D/C</th>
											<?php }else{ ?>
												<th class="text-nowrap">Send to Doctor - Call</th>
												<th class="text-nowrap">Call - Med D/C</th>
											<?php } ?>
											*/ ?>

											<th class="text-nowrap">Scan - Med D/C</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$n=0;
											$sumSendTime = 0;
											$sumSendMedTime = 0;
											$sumCallTime = 0;
											$sumMedDCTime = 0;
											foreach($Query->result() as $row) :
											if(!(intval($row->send_time_sum) < 1) || !(intval($row->call_time_sum) < 1) || !(intval($row->meddc_time_sum) < 1) ){
											$n++; 
											$sumSendTime += strtotime($row->send_time_sum);
											$sumCallTime += strtotime($row->call_time_sum);
											$sumMedDCTime += strtotime($row->meddc_time_sum);


											$time_SendTimeSum = explode(':',$row->send_time_sum);
											$time_CallTimeSum = explode(':',$row->call_time_sum);
											$time_MedDCTimeSum = explode(':',$row->meddc_time_sum);
											$totalmins_SendTimeSum = intval($time_SendTimeSum[0])*60 + intval($time_SendTimeSum[1]);
											$totalmins_CallTimeSum = intval($time_CallTimeSum[0])*60 + intval($time_CallTimeSum[1]);
											$totalmins_MedDCTimeSum = intval($time_MedDCTimeSum[0])*60 + intval($time_MedDCTimeSum[1]);

											$totalmins_ScanToCall = $totalmins_SendTimeSum + $totalmins_CallTimeSum;
											$totalmins_ScanToMed = $totalmins_SendTimeSum + $totalmins_CallTimeSum + $totalmins_MedDCTimeSum;

											$preH = ((int)($totalmins_ScanToMed / 60) > 9 ? "" : "0");
											$preM = ((int)($totalmins_ScanToMed % 60) > 9 ? "" : "0");
											$preS = ((int)($totalmins_ScanToMed % 3600/60) > 9 ? "" : "0");
											$time_ScanToMed = $preH . (int)($totalmins_ScanToMed / 60)  . ':' . $preM . (int)($totalmins_ScanToMed % 60) . ':' . $preS . (int)($totalmins_ScanToMed % 3600/60);
											$preH = ((int)($totalmins_ScanToCall / 60) > 9 ? "" : "0");
											$preM = ((int)($totalmins_ScanToCall % 60) > 9 ? "" : "0");
											$preS = ((int)($totalmins_ScanToCall % 3600/60) > 9 ? "" : "0");
											$time_ScanToDoctorCall = $preH . (int)($totalmins_ScanToCall / 60) . ':' . $preM . (int)($totalmins_ScanToCall % 60) . ':' . $preS . (int)($totalmins_ScanToCall % 3600/60);

										?>
										<tr>
											<td width="1%" class="f-s-600 text-inverse"><?=$n;?></td>
											<td><?=$row->careprovidername;?></td>
											<td><?=$row->send_time_sum;?></td>
											<?php if($this->session->userdata('ses_applocation') != '3'){ 
												$totalmins_SendToMed = $totalmins_CallTimeSum + $totalmins_MedDCTimeSum;
												$preH = ((int)($totalmins_SendToMed / 60) > 9 ? "" : "0");
												$preM = ((int)($totalmins_SendToMed % 60) > 9 ? "" : "0");
												$preS = ((int)($totalmins_SendToMed % 3600/60) > 9 ? "" : "0");
												$time_SendToMed = $preH . (int)($totalmins_SendToMed / 60)  . ':' . $preM . (int)($totalmins_SendToMed % 60) . ':' . $preS . (int)($totalmins_SendToMed % 3600/60);
											?>
												<td><?=$time_SendToMed;?></td>
											<?php } ?>
											<?php if($this->session->userdata('ses_applocation') == '3'){ 
												$sumSendMedTime += strtotime($row->send_meddc_time_sum);

												$time_SendToMedSum = explode(':',$row->send_meddc_time_sum);
												$totalmins_SendToMedSum = intval($time_SendToMedSum[0])*60 + intval($time_SendToMedSum[1]);

												$totalmins_ScanToMed = $totalmins_SendTimeSum + $totalmins_SendToMedSum;
												$preH = ((int)($totalmins_ScanToMed / 60) > 9 ? "" : "0");
												$preM = ((int)($totalmins_ScanToMed % 60) > 9 ? "" : "0");
												$preS = ((int)($totalmins_ScanToMed % 3600/60) > 9 ? "" : "0");
												$time_ScanToMed = $preH . (int)($totalmins_ScanToMed / 60)  . ':' . $preM . (int)($totalmins_ScanToMed % 60) . ':' . $preS . (int)($totalmins_ScanToMed % 3600/60);
											?>
												<td><?=$row->send_meddc_time_sum;?></td>
											<?php }else{ ?>
											<?php /*
												<td><?=$row->call_time_sum;?></td>
												<td><?=$row->meddc_time_sum;?></td>
											<?php */ ?>
											<?php } ?>
											<td><?=$time_ScanToMed;?></td>
										</tr>
										<?php } endforeach ?>
									</tbody>
								</table>
								<h3 style="padding:5px 5px 2px 15px;color: red; ">
									ระยะเวลารอพบแพทย์ทั้งหมด : <?=date('H:m:s',$sumSendTime);?> <br>

									<?php if($this->session->userdata('ses_applocation') == '3'){  ?>
										ระยะเวลาตรวจรวมทั้งหมด : <?=date('H:m:s',$sumSendMedTime);?> <br>
										ระยะเวลาตรวจเฉลี่ย : <?=date('H:m:s',$sumSendMedTime/$n);?> <br>
									<?php }else{ ?>
										ระยะเวลาเรียกจากแพทย์รวมทั้งหมด : <?=date('H:m:s',$sumCallTime);?>
										/ ระยะเวลาเรียกจากแพทย์เฉลี่ย : <?=date('H:m:s',$sumCallTime/$n);?> <br>
										ระยะเวลาตรวจเสร็จรวมทั้งหมด : <?=date('H:m:s',$sumMedDCTime);?>
										/ ระยะเวลาตรวจเฉลี่ย : <?=date('H:m:s',$sumMedDCTime/$n);?> <br>
									<?php } ?>

									จากแพทย์ทั้งหมด : <?=$n;?> คน
								</h4>
							</div>
							<!-- end panel-body -->