							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th width="1%"></th>
											<th class="text-nowrap">ชื่อแพทย์</th>
											<th class="text-nowrap">ระยะเวลาตรวจเสร็จ (Scan Q - Med D/C) </th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$n=0;
											$sumMedTime = 0;
											foreach($Query->result() as $row) :
											$n++; 
											$sumMedTime += strtotime($row->i_time_sum);
										?>
										<tr>
											<td width="1%" class="f-s-600 text-inverse"><?=$n;?></td>
											<td><?=$row->careprovidername;?></td>
											<td><?=$row->i_time_sum;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
								<h4 style="padding:5px 5px 2px 15px; ">
									ระยะเวลาตรวจทั้งหมด : <?=date('H:m:s',$sumMedTime);?> 
									ระยะเวลาเฉลี่ยต่อแพทย์ : <?=date('H:m:s',$sumMedTime/$n);?>
									จากแพทย์ทั้งหมด : <?=$n;?> คน
								</h4>
							</div>
							<!-- end panel-body -->