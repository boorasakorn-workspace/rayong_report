							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th class="text-nowrap">จำนวนคนไข้ทั้งหมด</th>
											<th class="text-nowrap">ชาย</th>
											<th class="text-nowrap">หญิง</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($Query->result() as $row) :
											$totalPatient = $row->i_sum_m + $row->i_sum_f;
										?>
										<tr>
											<td><?=$totalPatient;?></td>
											<td><?=$row->i_sum_m;?></td>
											<td><?=$row->i_sum_f;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<!-- end panel-body -->