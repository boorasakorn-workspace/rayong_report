							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th width="1%"></th>
											<th class="text-nowrap">ปี</th>
											<th class="text-nowrap">จำนวนคนไข้</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($Query->result() as $row) :
										?>
										<tr>
											<td><?=$row->i_year;?></td>
											<td><?=$row->i_sum;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<!-- end panel-body -->