							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th width="1%"></th>
											<th class="text-nowrap" width="30%">ชื่อแพทย์</th>
											<th class="text-nowrap" width="20%">ทั่วไป</th>
											<th class="text-nowrap" width="20%">นัดหมาย</th>
											<th class="text-nowrap" width="29%">จำนวนคนไข้</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$n = 0;
											$sumPatient = 0;
											foreach($Query->result() as $row) :
											$n++; 
											$sumPatient += $row->i_m_sum+$row->i_f_sum;
										?>
										<tr>
											<td width="1%" class="f-s-600 text-inverse"><?=$n;?></td>
											<td><?=$row->careprovidername;?></td>
											<td><?=($row->patient_walkin != '' ? $row->patient_walkin : "0");?></td>
											<td><?=($row->patient_appoint != '' ? $row->patient_appoint : "0");?></td>
											<td><?=$row->patient_walkin+$row->patient_appoint;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
								<h4 style="padding:5px 5px 2px 15px; ">
									ยอดคนไข้รวมทั้งหมด : <?=$sumPatient;?> 
									ยอดคนไข้เฉลี่ยต่อแพทย์ : <?=intval($sumPatient/$n);?> คน จากแพทย์ทั้งหมด <?=$n;?> คน
								</h4>
							</div>
							<!-- end panel-body -->