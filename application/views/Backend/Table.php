<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Color Admin | Managed Tables - Extension Combination</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />

	<?php 
	echo assets_css('app.min.css');
	echo assets_css('datatables/dataTables.bootstrap4.min.css');
	echo assets_css('datatables/responsive.bootstrap4.min.css');
	echo assets_css('datatables/buttons.bootstrap4.min.css');
	?>
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		
		<?php
			$this->view($headercontent); 

			$this->view($sidebarcontent);
		?>		

		<?php if(isset($TotalPatient) && $TotalPatient->num_rows() > 0){ ?>
			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;">Tables Report</a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">TotalPatient</h1>
				<!-- end page-header -->
				<!-- begin row -->
				<div class="row">
					<!-- begin col-12 -->
					<div class="col-xl-12">
						<div class="panel panel-inverse">
							<!-- begin panel-heading -->
							<div class="panel-heading">
								<h4 class="panel-title">TotalPatient</h4>
								<div class="panel-heading-btn">
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<!-- end panel-heading -->
							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th class="text-nowrap">จำนวนคนไข้ทั้งหมด</th>
											<th class="text-nowrap">ชาย</th>
											<th class="text-nowrap">หญิง</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($TotalPatient->result() as $row) :
											$totalPatient = $row->i_sum_m + $row->i_sum_f;
										?>
										<tr>
											<td><?=$totalPatient;?></td>
											<td><?=$row->i_sum_m;?></td>
											<td><?=$row->i_sum_f;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<!-- end panel-body -->
						</div>
					</div>
					<!-- end col-10 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end #content -->

			<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
			<!-- end scroll to top btn -->
		<?php } ?>

		<?php if(isset($LocateTotalPatient) && $LocateTotalPatient->num_rows() > 0){ ?>
			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;">Tables Report</a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">LocateTotalPatient</h1>
				<!-- end page-header -->
				<!-- begin row -->
				<div class="row">
					<!-- begin col-12 -->
					<div class="col-xl-12">
						<div class="panel panel-inverse">
							<!-- begin panel-heading -->
							<div class="panel-heading">
								<h4 class="panel-title">LocateTotalPatient</h4>
								<div class="panel-heading-btn">
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<!-- end panel-heading -->
							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th class="text-nowrap">จำนวนคนไข้ทั้งหมด</th>
											<th class="text-nowrap">ชาย</th>
											<th class="text-nowrap">หญิง</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($LocateTotalPatient->result() as $row) :
											$totalPatient = $row->i_sum_m + $row->i_sum_f;
										?>
										<tr>
											<td><?=$totalPatient;?></td>
											<td><?=$row->i_sum_m;?></td>
											<td><?=$row->i_sum_f;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<!-- end panel-body -->
						</div>
					</div>
					<!-- end col-10 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end #content -->

			<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
			<!-- end scroll to top btn -->
		<?php } ?>

		<?php if(isset($PatientDaysStat) && $PatientDaysStat->num_rows() > 0){ ?>
			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;">Tables Report</a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">PatientDaysStat</h1>
				<!-- end page-header -->
				<!-- begin row -->
				<div class="row">
					<!-- begin col-12 -->
					<div class="col-xl-12">
						<div class="panel panel-inverse">
							<!-- begin panel-heading -->
							<div class="panel-heading">
								<h4 class="panel-title">PatientDaysStat</h4>
								<div class="panel-heading-btn">
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<!-- end panel-heading -->
							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th width="1%"></th>
											<th class="text-nowrap">resultdate</th>
											<th class="text-nowrap">ชื่อแพทย์</th>
											<th class="text-nowrap">ชาย</th>
											<th class="text-nowrap">หญิง</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$n = 0;
											foreach($PatientDaysStat->result() as $row) :
											$n++; 
										?>
										<tr>
											<td width="1%" class="f-s-600 text-inverse"><?=$n;?></td>
											<td><?=$row->resultdate;?></td>
											<td><?=$row->careprovidername;?></td>
											<td><?=$row->i_m_sum;?></td>
											<td><?=$row->i_f_sum;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<!-- end panel-body -->
						</div>
					</div>
					<!-- end col-10 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end #content -->

			<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
			<!-- end scroll to top btn -->
		<?php } ?>

		<?php if(isset($CountPatientYear) && $CountPatientYear->num_rows() > 0){ ?>
			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;">Tables Report</a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">CountPatientYear</h1>
				<!-- end page-header -->
				<!-- begin row -->
				<div class="row">
					<!-- begin col-12 -->
					<div class="col-xl-12">
						<div class="panel panel-inverse">
							<!-- begin panel-heading -->
							<div class="panel-heading">
								<h4 class="panel-title">CountPatientYear</h4>
								<div class="panel-heading-btn">
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<!-- end panel-heading -->
							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th width="1%"></th>
											<th class="text-nowrap">ปี</th>
											<th class="text-nowrap">จำนวนคนไข้</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach($CountPatientYear->result() as $row) :
										?>
										<tr>
											<td><?=$row->i_year;?></td>
											<td><?=$row->i_sum;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<!-- end panel-body -->
						</div>
					</div>
					<!-- end col-10 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end #content -->

			<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
			<!-- end scroll to top btn -->
		<?php } ?>

		<?php if(isset($MedScanStat) && $MedScanStat->num_rows() > 0){ ?>
			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;">Tables Report</a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">MedScanStat</h1>
				<!-- end page-header -->
				<!-- begin row -->
				<div class="row">
					<!-- begin col-12 -->
					<div class="col-xl-12">
						<div class="panel panel-inverse">
							<!-- begin panel-heading -->
							<div class="panel-heading">
								<h4 class="panel-title">MedScanStat</h4>
								<div class="panel-heading-btn">
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<!-- end panel-heading -->
							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th width="1%"></th>
											<th class="text-nowrap">ชื่อแพทย์</th>
											<th class="text-nowrap">Scan Q - Med D/C</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$n=0;
											foreach($MedScanStat->result() as $row) :
											$n++; 
										?>
										<tr>
											<td width="1%" class="f-s-600 text-inverse"><?=$n;?></td>
											<td><?=$row->careprovidername;?></td>
											<td><?=$row->i_time_sum;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<!-- end panel-body -->
						</div>
					</div>
					<!-- end col-10 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end #content -->

			<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
			<!-- end scroll to top btn -->
		<?php } ?>

		<?php if(isset($AllProcessTime) && $AllProcessTime->num_rows() > 0){ ?>
			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;">Tables Report</a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header">AllProcessTime</h1>
				<!-- end page-header -->
				<!-- begin row -->
				<div class="row">
					<!-- begin col-12 -->
					<div class="col-xl-12">
						<div class="panel panel-inverse">
							<!-- begin panel-heading -->
							<div class="panel-heading">
								<h4 class="panel-title">AllProcessTime</h4>
								<div class="panel-heading-btn">
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<!-- end panel-heading -->
							<!-- begin panel-body -->
							<div class="panel-body">
								<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
									<thead>
										<tr>
											<th width="1%"></th>
											<th class="text-nowrap">ชื่อแพทย์</th>
											<th class="text-nowrap">Scan - Send to Doctor</th>
											<th class="text-nowrap">Send to Doctor - Call</th>
											<th class="text-nowrap">Call - Med D/C</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											$n=0;
											foreach($AllProcessTime->result() as $row) :
											$n++; 
										?>
										<tr>
											<td width="1%" class="f-s-600 text-inverse"><?=$n;?></td>
											<td><?=$row->careprovidername;?></td>
											<td><?=$row->send_time_sum;?></td>
											<td><?=$row->call_time_sum;?></td>
											<td><?=$row->meddc_time_sum;?></td>
										</tr>
										<?php endforeach ?>
									</tbody>
								</table>
							</div>
							<!-- end panel-body -->
						</div>
					</div>
					<!-- end col-10 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end #content -->

			<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
			<!-- end scroll to top btn -->
		<?php } ?>

	</div>
	<!-- end page container -->
	<?php
	echo assets_js('app.min.js');
	echo assets_js('default.min.js');
	echo assets_js('datatables/jquery.dataTables.min.js');
	echo assets_js('datatables/dataTables.bootstrap4.min.js');
	echo assets_js('datatables/dataTables.responsive.min.js');
	echo assets_js('datatables/responsive.bootstrap4.min.js');
	echo assets_js('datatables/dataTables.buttons.min.js');
	echo assets_js('datatables/buttons.bootstrap4.min.js');
	echo assets_js('datatables/buttons.colVis.min.js');
	echo assets_js('datatables/buttons.flash.min.js');
	echo assets_js('datatables/buttons.html5.min.js');
	echo assets_js('datatables/buttons.print.min.js');
	echo assets_js('pdf/pdfmake.min.js');
	echo assets_js('pdf/vfs_fonts.js');
	echo assets_js('zip/jszip.min.js');
	echo assets_js('table-manage.js');

	?>
</body>
</html>
