<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Report Rayong Dashboard</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<?php 
		echo assets_css('fonts/kanit/stylesheet.css');
		echo assets_css('app.min.css');
		echo assets_css('bootstrap-datepicker.css');
		echo assets_css('chartjs/slide-chartcss.css');
	?>

	<?php /*echo assets_css('jquery.gritter.css')*/;?>
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		
		<?php $this->view($headercontent); ?>

		<?php $this->view($sidebarcontent); ?>
		
		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin breadcrumb -->
			<ol class="breadcrumb float-xl-right">
				<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
				<li class="breadcrumb-item active">Dashboard</li>
			</ol>
			<!-- end breadcrumb -->
			<!-- begin page-header -->
			<h1 class="page-header">Dashboard</h1>
			<!-- end page-header -->
			<!-- begin row -->
			<div class="row">
				<!-- begin col-8 -->
				<div class="col-xl-6">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="index-1">
						<div class="panel-heading">
							<h4 class="panel-title">Details</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="panel-body pr-1">
							<div id="interactive-chart" class="height-sm">
								<?php $this->view($graphChart); ?>
							</div>
						</div>
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-8 -->
				<!-- begin col-4 -->
				<div class="col-xl-6">
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="index-7">
						<div class="panel-heading">
							<h4 class="panel-title">จำนวนคนไข้ ของแผนก <?=$this->session->userdata('ses_locationtitle');?> ประจำวัน</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<?php 
							if(isset($LocateTotalPatient) && $LocateTotalPatient->num_rows() > 0){
								$data['Query'] = $LocateTotalPatient;
								$this->load->view('Backend/tableReportBody/LocateTotalPatient', $data);
							}
							else{
						?>
							<div class="panel-body">							
								ไม่พบข้อมูล
							</div>
						<?php
							}
						?>
						<!--
						<div class="panel-body">							
							<div id="donut-chart" class="height-sm"></div>
						</div>
						-->
					</div>
					<!-- end panel -->
				</div>
				<!-- end col-4 -->
			</div>
			<!-- end row -->
		</div>
		<!-- end #content -->
		
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->
	<?php
	echo assets_js('app.min.js');
	echo assets_js('default.min.js');
	// echo assets_js('jquery.gritter.js');
	echo assets_js('bootstrap-datepicker.js');
	echo assets_js('notify.min.js');
	echo assets_js('dashboard.js');
	echo assets_js('chartjs/chart-apex.demo.js');
	echo assets_js('chartjs/graph-charts.min.js');
	echo assets_js('chartjs/slide-chartjs.js');
	?>
	<script type="text/javascript" charset="utf-8">
		$( document ).ready(function() {
			if( "<?=$this->session->flashdata('logResult');?>" != ""){
				$.notify(
					"<?=$this->session->flashdata('logResult');?>",
					{position:"top center",className:"success"}
				);				
			}
			//Temporary Draw Chart
			var options = {
				chart: {
					height: 350,
					type: 'line',
					stacked: false
				},
				dataLabels: {
					enabled: false
				},
				series: [
						{
							name: 'Male',
							type: 'column',
							data: [<?php foreach($GraphLocalTotalPatient->result() as $graphrow) : ?><?=$graphrow->i_sum_m;?>,<?php endforeach ?>]
						},
						{
							name: 'Female',
							type: 'column',
							data: [<?php foreach($GraphLocalTotalPatient->result() as $graphrow) : ?><?=$graphrow->i_sum_f;?>,<?php endforeach ?>]
						},
						{
							name: 'Total',
							type: 'column',
							data: [<?php foreach($GraphLocalTotalPatient->result() as $graphrow) : ?><?=$graphrow->i_sum_m+$graphrow->i_sum_f;?>,<?php endforeach ?>]
						},
				],
				stroke: {
					width: [0, 0, 3]
				},
				colors: [COLOR_BLUE_DARKER, COLOR_TEAL, COLOR_ORANGE],
				title: {
					text: 'Total Patient in Last 7 Days',
					align: 'left',
					offsetX: 110
				},
				xaxis: {
					categories: [<?php foreach($GraphLocalTotalPatient->result() as $graphrow) : ?>'<?=date("m-d", strtotime($graphrow->resultdate));?>',<?php endforeach ?>],
					axisBorder: {
						show: true,
						color: COLOR_SILVER_TRANSPARENT_5,
						height: 1,
						width: '100%',
						offsetX: 0,
						offsetY: -1
					},
					axisTicks: {
						show: true,
						borderType: 'solid',
						color: COLOR_SILVER,
						height: 6,
						offsetX: 0,
						offsetY: 0
					}
				},
				tooltip: {
					fixed: {
						enabled: true,
						position: 'topLeft', // topRight, topLeft, bottomRight, bottomLeft
						offsetY: 30,
						offsetX: 60
					},
				},
				legend: {
					horizontalAlign: 'left',
					offsetX: 40
				}
			};

			var chart = new ApexCharts(
				document.querySelector('#apex-mixed-chart'),
				options
			);

			chart.render();
			//End Draw
		
		});


	</script>
</body>
</html>
