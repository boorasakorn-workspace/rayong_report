
					<!-- begin panel -->
					<div class="panel panel-inverse" data-sortable-id="index-6">
						<div class="panel-heading">
							<h4 class="panel-title">Details</h4>
							<div class="panel-heading-btn">
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
								<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="widget widget-stats bg-blue">
								<div class="stats-icon"><i class="fa fa-desktop"></i></div>
									<div class="stats-info">
										<h4>TOTAL VISITORS</h4>
										<p>3,291,922</p>	
									</div>
									<div class="stats-link">
										<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="widget widget-stats bg-info">
									<div class="stats-icon"><i class="fa fa-link"></i></div>
									<div class="stats-info">
										<h4>BOUNCE RATE</h4>
										<p>20.44%</p>	
									</div>
									<div class="stats-link">
										<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="widget widget-stats bg-orange">
									<div class="stats-icon"><i class="fa fa-users"></i></div>
									<div class="stats-info">
										<h4>UNIQUE VISITORS</h4>
										<p>1,291,922</p>	
									</div>
									<div class="stats-link">
										<a href="javascript:;">View Detail <i class="fa fa-arrow-alt-circle-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- end panel -->