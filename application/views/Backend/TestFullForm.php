<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Table Report | <?=$Topic;?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<?php 
	echo assets_css('app.min.css');
	echo assets_css('bootstrap-datepicker.css');
	?>
</head>
<body>
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		
		<?php $this->view($headercontent); ?>

		<?php $this->view($sidebarcontent); ?>

			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;">Table Report : <?=$Topic;?></a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header"><?=$Topic;?></h1>
				<!-- end page-header -->
				<!-- begin row -->
				<div class="row">
					<!-- begin col-12 -->
					<div class="col-xl-12">
						<div class="panel panel-inverse">
							<!-- begin panel-heading -->
							<div class="panel-heading">
								<h4 class="panel-title"><?=$Topic;?></h4>
								<div class="panel-heading-btn">
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<!-- end panel-heading -->

							<!-- begin panel-body -->
							<div class="panel-body panel-form">
								<form class="form-horizontal form-bordered">
									<div class="form-group row">
										<!-- <label class="col-lg-4 col-form-label"> Topic </label> -->
										<div class="col-lg-6">
											<div class="input-group input-daterange">
												<input type="text" class="form-control" name="start" placeholder="Date Start"/>
												<span class="input-group-addon">to</span>
												<input type="text" class="form-control" name="end" placeholder="Date End"/>
											</div>
										</div>
										<div class="col-lg-2">
											<button type="submit" class="btn-success btn-sm"> ดูรายงาน </button>
										</div>
									</div>
								</form>
							</div>
							<!-- end panel-body -->

							<?php if(isset($Query) && $Query->num_rows() > 0){ ?>
								<!-- begin panel-body -->
								<div class="panel-body">
									<table id="data-table-combine" class="table table-striped table-bordered table-td-valign-middle">
										<thead>
											<tr>
												<th width="1%"></th>
												<th class="text-nowrap">resultdate</th>
												<th class="text-nowrap">ชื่อแพทย์</th>
												<th class="text-nowrap">ชาย</th>
												<th class="text-nowrap">หญิง</th>
											</tr>
										</thead>
										<tbody>
											<?php 
												$n = 0;
												foreach($Query->result() as $row) :
												$n++; 
											?>
											<tr>
												<td width="1%" class="f-s-600 text-inverse"><?=$n;?></td>
												<td><?=$row->resultdate;?></td>
												<td><?=$row->careprovidername;?></td>
												<td><?=$row->i_m_sum;?></td>
												<td><?=$row->i_f_sum;?></td>
											</tr>
											<?php endforeach ?>
										</tbody>
									</table>
								</div>
								<!-- end panel-body -->
							<?php } ?>
						</div>
					</div>
					<!-- end col-10 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end #content -->

			<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
			<!-- end scroll to top btn -->
		
        
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->	
	<?php
	echo assets_js('app.min.js');
	echo assets_js('default.min.js');
	echo assets_js('bootstrap-datepicker.js');
	echo assets_js('form-plugins.demo.js');
	echo assets_js('datatables/jquery.dataTables.min.js');
	echo assets_js('datatables/dataTables.bootstrap4.min.js');
	echo assets_js('datatables/dataTables.responsive.min.js');
	echo assets_js('datatables/responsive.bootstrap4.min.js');
	echo assets_js('datatables/dataTables.buttons.min.js');
	echo assets_js('datatables/buttons.bootstrap4.min.js');
	echo assets_js('datatables/buttons.colVis.min.js');
	echo assets_js('datatables/buttons.flash.min.js');
	echo assets_js('datatables/buttons.html5.min.js');
	echo assets_js('datatables/buttons.print.min.js');
	echo assets_js('pdf/pdfmake.min.js');
	echo assets_js('pdf/vfs_fonts.js');
	echo assets_js('zip/jszip.min.js');
	echo assets_js('table-manage.js');
	?>
	<script type="text/javascript">
		$('.input-daterange input').datepicker({
			format: 'dd/mm/yyyy',
		});
	</script>
</body>
</html>
