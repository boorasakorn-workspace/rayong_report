
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		
		<?php $this->view($headercontent); ?>

		<?php $this->view($sidebarcontent); ?>

			<!-- begin #content -->
			<div id="content" class="content">
				<!-- begin breadcrumb -->
				<ol class="breadcrumb float-xl-right">
					<li class="breadcrumb-item"><a href="javascript:;">Home</a></li>
					<li class="breadcrumb-item"><a href="javascript:;">Table Report : <?=$Topic;?></a></li>
				</ol>
				<!-- end breadcrumb -->
				<!-- begin page-header -->
				<h1 class="page-header"><?=$Topic;?></h1>
				<!-- end page-header -->
				<!-- begin row -->
				<div class="row">
					<!-- begin col-12 -->
					<div class="col-xl-12">
						<div class="panel panel-inverse">
							<!-- begin panel-heading -->
							<div class="panel-heading">
								<h4 class="panel-title"><?=$Topic;?></h4>
								<div class="panel-heading-btn">
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
									<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
								</div>
							</div>
							<!-- end panel-heading -->
							
							<!---------------------------- CONTENT ----------------------------->

								<?php if(isset($hasDatePicker) && $hasDatePicker > 0){ ?>
									<!-- begin panel-body -->
									<div class="panel-body panel-form">
										<form class="form-horizontal form-bordered">
											<div class="form-group row">
												<!-- <label class="col-lg-4 col-form-label"> Topic </label> -->
												<div class="col-lg-6">
													<div class="input-group input-daterange">
														<input type="text" class="form-control" name="start" placeholder="Date Start"/>
														<span class="input-group-addon">to</span>
														<input type="text" class="form-control" name="end" placeholder="Date End"/>
													</div>
												</div>
												<div class="col-lg-2">
													<button type="submit" class="btn-success btn-sm"> ดูรายงาน </button>
												</div>
											</div>
										</form>
									</div>
									<!-- end panel-body -->
									<h4 style="padding:5px 5px 2px 15px; ">
										<?php if( $this->input->get('start',TRUE) !== NULL && $this->input->get('end',TRUE) !== NULL ){ ?>
											ข้อมูลจากวันที่ <?=$this->input->get('start',TRUE);?> ถึงวันที่ <?=$this->input->get('end',TRUE);?>
										<?php }
										else{ ?>
											ข้อมูลของวันที่ <?=date('d/m/Y');?>
										<?php } ?>
									</h4>
								<?php } ?>
								<?php 
									if(isset($Query) && $Query->num_rows() > 0){
										$data['Query'] = $Query;
										$this->load->view('Backend/tableReportBody/'.$BodyContent, $data);
									}
									else{
										echo "<h5>ไม่พบข้อมูล</h5>";
									}
								?>

							<!---------------------------- CONTENT ----------------------------->

						</div>
					</div>
					<!-- end col-10 -->
				</div>
				<!-- end row -->
			</div>
			<!-- end #content -->

			<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
			<!-- end scroll to top btn -->
		
        
		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->	