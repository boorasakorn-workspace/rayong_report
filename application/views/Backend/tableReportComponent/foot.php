	<?php
	echo assets_js('app.min.js');
	echo assets_js('default.min.js');
	echo assets_js('bootstrap-datepicker.js');
	echo assets_js('form-plugins.demo.js');
	echo assets_js('datatables/jquery.dataTables.min.js');
	echo assets_js('datatables/dataTables.bootstrap4.min.js');
	echo assets_js('datatables/dataTables.responsive.min.js');
	echo assets_js('datatables/responsive.bootstrap4.min.js');
	echo assets_js('datatables/dataTables.buttons.min.js');
	echo assets_js('datatables/buttons.bootstrap4.min.js');
	echo assets_js('datatables/buttons.colVis.min.js');
	echo assets_js('datatables/buttons.flash.min.js');
	echo assets_js('datatables/buttons.html5.min.js');
	echo assets_js('datatables/buttons.print.min.js');
	echo assets_js('pdf/pdfmake.min.js');
	echo assets_js('pdf/vfs_fonts.js');
	echo assets_js('zip/jszip.min.js');
	echo assets_js('table-manage.js');
	?>
	<script type="text/javascript">
		$('.input-daterange input').datepicker({
			format: 'dd/mm/yyyy',
		});
	</script>
</body>
</html>