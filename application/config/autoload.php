<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$autoload['packages'] = array(APPPATH.'third_party');

$autoload['libraries'] = array('database','session');

$autoload['drivers'] = array();

$autoload['helper'] = array('url','html','form','security');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array();
